import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { Router } from "@angular/router";
import { Chart } from "angular-highcharts";
import { ListenerService } from "../Service/Listener.service";
import { Subscription } from "rxjs";
import { AppComponent } from "../app.component";

@Component({
  selector: "app-Panel7",
  templateUrl: "./Panel7.component.html",
  styleUrls: ["./Panel7.component.scss"]
})
export class Panel7Component implements OnInit {
  private subscription: Subscription;

  url = this.parent.url;

  //modal
  // @ViewChild('timePickerContainer') public timePickerContainer;
  @ViewChild("articlesContainer") public articlesContainer;
  // @ViewChild('mediaContainer') public mediaContainer;

  //filter parameter
  today = new Date();
  periodFromModel: Date = this.addDays(this.today, -7);
  periodEndModel: Date = this.today;
  wordcloud = new Chart({});
  loaded = false;
  report_val = false;

  //fitlerparam
  category_set = "0";
  user_media_type_id = "0";
  timeFrame = 2;

  //param Socmed
  socmed1;
  socmed2;

  // lineChart
  public lineChartData: Array<any>;
  public lineChartLabels: Array<any>;
  public lineChartColors: Array<any>;
  public lineChartOptions: any = {
    responsive: true,
    // bezierCurve: false,
    maintainAspectRatio: false,
    legend: {
      display: true,
      position: "bottom",
      labels: {
        fontColor: "whitesmoke"
      }
    },
    scales: {
      xAxes: [
        {
          gridLines: {
            display: true
          },
          ticks: {
            display: false
          }
        }
      ],
      yAxes: [
        {
          gridLines: {
            drawBorder: false
          },
          ticks: {
            min: 0,
            max: 15,
            stepSize: 5,
            userCallback: function(value) {
              if (value < 5) {
                return "Potential";
              } else if (value >= 5 && value < 10) {
                return "Emerging";
              } else if (value >= 10 && value < 15) {
                return "Current";
              } else if ((value = 15)) {
                return "Crisis";
              }
            }
          }
        }
      ]
    }
  };

  //issue life cycle
  gaugeType = "semi";
  gaugeValue = 28.3;
  gaugeLabel = "Potential";
  gaugeLabel2 = "Emerging";
  gaugeLabel3 = "Current";
  gaugeLabel4 = "Crisis";
  gaugeAppendText = "%";

  totalEWS = 0;
  totalPotential = 0;
  totalEmerging = 0;
  totalCurrent = 0;
  totalCrisis = 0;

  public lineChartLegend: boolean = true;
  public lineChartType: string = "line";

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }

  onKey(res: any) {
    // console.log(res);
    if (res.code == "ArrowLeft") {
      this.router.navigateByUrl("/panel6");
    } else if (res.code == "ArrowRight") {
      this.router.navigateByUrl("/panel8");
    } else if (res.code == "ArrowDown") {
      this.router.navigateByUrl("/panelDesc");
    }
  }

  public chartClicked(e: any): void {
    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if (activePoints.length > 0) {
        // get the internal index of slice in pie chart
        const clickedElementIndex = activePoints[0]._index;
        const clickDatasetsIndex = activePoints[0]._datasetIndex;
        const label = moment(chart.data.labels[clickedElementIndex])
          .format("YYYY-MM-DD")
          .toString();

        const value =
          chart.data.datasets[clickDatasetsIndex].data[clickedElementIndex];
        const category = chart.data.datasets[clickDatasetsIndex].label;
        let start = label;
        let end = label;
        if (this.timeFrame == 4) {
          var dateConverted = moment(label).toDate();
          dateConverted = this.addDays(dateConverted, 30);
          end = moment(dateConverted)
            .format("YYYY-MM-DD")
            .toString();
        }
        this.articlesContainer.loadArticles(
          this.category_set,
          this.user_media_type_id,
          "0",
          category,
          start,
          end,
          1,
          null
        );
      }
    }
  }

  updatefun() {
    interface UserResponse {
      data: Object;
    }

    this.http
      .get<UserResponse>("https://kemenlu.digivla.id/api/update")
      .subscribe(
        (result: any) => {},
        err => {}
      );
  }

  getMediaGroup() {
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }
      this.http
        .get<UserResponse>(this.url + "user/medias/", this.parent.options)
        .subscribe((result: any) => {
          this.user_media_type_id = result.results[0].user_media_type_id;
          resolve(result);
        });
    });
  }

  loadEWS() {
    this.loaded = false;
    interface UserResponse {
      data: Object;
    }
    let editingParam = {
      category_set: this.category_set,
      category_id: "all",
      user_media_type_id: this.user_media_type_id,
      media_id: "0",
      start_date: moment(this.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/tone-by-media-tier",
        editingParam,
        this.parent.options
      )
      .subscribe((result: any) => {
        let dataGraph = [];
        let xAxisGraph = [];
        let graphColor = [];

        var xAxisChart = [];
        var chartData = [];
        for (let i = 0; i < result.data.length; i++) {
          for (let j = 0; j < result.data[i].dates.length; j++) {
            var a = xAxisChart.indexOf(result.data[i].dates[j].date);
            // console.log(a);
            if (a < 0) {
              xAxisChart.push(result.data[i].dates[j].date);
            }
          }
        }

        var category_id = [];
        for (let i = 0; i < result.data.length; i++) {
          var datesArr = [];
          for (let j = 0; j < result.data[i].dates.length; j++) {
            var positive = 0;
            var negative = 0;
            for (let k = 0; k < result.data[i].dates[j].tones.length; k++) {
              if (result.data[i].dates[j].tones[k].tonality > 0) {
                positive = result.data[i].dates[j].tones[k].summary_by_tier;
              } else if (result.data[i].dates[j].tones[k].tonality < 0) {
                negative = result.data[i].dates[j].tones[k].summary_by_tier;
              }
            }
            var val = positive - negative;
            if (val < 0) {
              val = 0 - val;
              if (val > 15) {
                val = 15;
              }
            } else if (negative > 0) {
              val = 1;
            } else {
              val = 0;
            }
            datesArr.push(val);
          }
          category_id.push({
            label: result.data[i].category_id,
            data: datesArr
          });
          var color = this.randomNonRedColor();
          graphColor.push({
            borderColor: color,
            pointBorderColor: "#fff",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: color
          });
        }

        // setTimeout(()=>{ this.mouseEnter('time-frame-'+this.timeFrame)},1000);

        this.lineChartData = [];
        this.lineChartData = category_id;
        // console.log(this.lineChartData);
        this.lifeCyclecounter();
        this.lineChartLabels = [];
        this.lineChartLabels = xAxisChart;
        this.lineChartColors = [];
        this.lineChartColors = graphColor;
        this.loaded = true;
      });
  }

  lifeCyclecounter() {
    this.totalPotential = 0;
    this.totalEmerging = 0;
    this.totalCurrent = 0;
    this.totalCrisis = 0;
    this.totalEWS = 0;
    for (let i = 0; i < this.lineChartData.length; i++) {
      var data = this.lineChartData[i].data;
      // console.log(data);
      for (let j = 0; j < data.length; j++) {
        this.totalEWS = this.totalEWS + data[j];
        if (data[j] < 5) {
          this.totalPotential = this.totalPotential + data[j];
        } else if (data[j] > 4 && data[j] < 10) {
          this.totalEmerging = this.totalEmerging + data[j];
        } else if (data[j] > 9 && data[j] < 15) {
          this.totalCurrent = this.totalCurrent + data[j];
        } else if (data[j] > 14) {
          this.totalCrisis = this.totalCrisis + data[j];
        }
      }
    }

    // console.log(this.totalPotential);
    var potential = this.countPercent(this.totalPotential, this.totalEWS);
    var emerging = this.countPercent(this.totalEmerging, this.totalEWS);
    var current = this.countPercent(this.totalCurrent, this.totalEWS);
    var crisis = this.countPercent(this.totalCrisis, this.totalEWS);

    this.totalPotential = +potential;
    this.totalEmerging = +emerging;
    this.totalCurrent = +current;
    this.totalCrisis = +crisis;

    // console.log(potential)
  }

  reporthandle() {
    // alert('test')
    this.base64Converter().then(res => {
      
      // base.push({base64:res});
      //console.log(base64);

      // let breakdown = this.socmed2.data_page1.breakdown;
      // const socmed_total = breakdown.Blog + breakdown.Facebook + breakdown.Forum + breakdown.Instagram + breakdown.News + breakdown.Twitter + breakdown.Video + breakdown.Web;
      // mb = Object.entries(breakdown).map(([name, count_original]) => ({name,count_original}));
      // let mb_details={};
      // for (let i=0;i<mb.length;i++){
      //   mb[i].count = this.kFormatter(mb[i].count_original);
      //   mb[i].percent = this.countPercent(mb[i].count_original,socmed_total);
      //   mb[i].icon = mb[i].name;
      //   mb[i].name_original = mb[i].name;
      //   // mb_details.push(mb[i]);
      //   mb_details[i] = mb[i];
      // }

      // let top_post = [];
      // let araypost=[]
      // // let top_posts = {};
      // top_post = Object.entries(this.socmed2.data_page2.top_posts).map(([key,value]) => (value));
      // // Object.entries(top_post).forEach(([key, value]) => araypost.push({value})); // "foo: bar", "baz: 42"

      // // top_post = Object.entries(top_post).map(([key,value]) => ({value}));
      // // top_post = Object.keys(this.socmed2.data_page2.top_posts).map(function(key) {
      // //   return this.socmed2.data_page2.top_posts[key];
      // // });
      // // console.log(this.socmed2.data_page2.top_posts);
      // // for (let i =0;i < top_post.length;i++){
      // //   top_posts[i]=top_post[i].value;
      // // }

      // // console.log(top_post);

      // param.push({
      //   subject: 'Overview Report for ' + today,
      //   data:{
      //     images:{wordcloud:base[0]},
      //     stats:{
      //       mb:mb_details,
      //       tm:this.kFormatter(socmed_total),
      //       ts:top_post
      //     },
      //     client:"Kemlu"
      //   },
      //   to: this.parent.email
      // })
      // console.log(param)

      // console.log(JSON.stringify(param[0]));
      let param = {
        clientid: this.parent.socmed,
        to: this.parent.email,
        wc_image: res
      };
      interface UserResponse {
        data: Object;
      }
      //
      this.http
        .post<UserResponse>("https://report.digivla.id/generatereport", param)
        .subscribe(
          (result: any) => {
            alert("Report Sent");
            // console.log(result);
          },
          err => {}
        );
    });
  }

  countPercent(val, total) {
    var a = (val / total) * 100;

    return a.toFixed();
  }

  kFormatter(num) {
    var suffixes = ["", "K", "M", "B", "T"];
    var suffixNum = Math.floor(("" + num).length / 3);
    suffixNum = suffixNum > 1 ? suffixNum - 1 : suffixNum;
    var shortValue = parseFloat(
      (suffixNum != 0 ? num / Math.pow(1000, suffixNum) : num).toPrecision(2)
    );
    // console.log(num)
    // console.log(suffixNum)
    // console.log(Math.pow(1000,suffixNum))
    // console.log(shortValue)
    if (shortValue % 1 != 0) {
      var shortNum = shortValue.toFixed(1);
    }
    return shortValue + suffixes[suffixNum];
  }

  base64Converter() {
    return new Promise(resolve => {
      var className = document
        .getElementById("hidenDiv")
        .querySelectorAll("svg");
      if (className.length > 0) {
        var result = new XMLSerializer().serializeToString(className[0]);
        var base64str =
          "data:image/svg+xml;base64," +
          btoa(unescape(encodeURIComponent(result)));

        let im = new Image();
        im.src = base64str;

        let can = document.createElement("canvas"); // Not shown on page
        let ctx = can.getContext("2d");

        let self = this;
        im.onload = function() {
          can.width = im.width;
          can.height = im.height;
          ctx.drawImage(im, 0, 0, im.width, im.height);

          resolve(can.toDataURL("image/png"));
        };
      }
    });
  }

  loadSocmed1() {
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }

      this.http
        .get<UserResponse>(
          "https://kemenlu.digivla.id/api/wordcloud/" + this.parent.socmed
        )
        .subscribe(
          (result: any) => {
            this.socmed1 = result.data;
            resolve(this.socmed1);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  loadSocmed2() {
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }

      this.http
        .get<UserResponse>(
          "https://kemenlu.digivla.id/api/onlinemonitoring/" +
            this.parent.socmed
        )
        .subscribe(
          (result: any) => {
            this.socmed2 = result.data;
            resolve(this.socmed2);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  randomNonRedColor = function() {
    //skip green
    var ranges = [
      [20, 120],
      [150, 300]
    ];

    //get max random
    var total = 0,
      i;
    for (i = 0; i < ranges.length; i += 1) {
      total += ranges[i][1] - ranges[i][0] + 1;
    }

    //get random hue index
    var randomHue = Math.floor(Math.random() * total);

    //convert index to actual hue
    var pos = 0;
    for (i = 0; i < ranges.length; i += 1) {
      pos = ranges[i][0];
      if (randomHue + pos <= ranges[i][1]) {
        randomHue += pos;
        break;
      } else {
        randomHue -= ranges[i][1] - ranges[i][0] + 1;
      }
    }

    return this.hslToHex(randomHue, 100, 50);
  };

  hslToHex(h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;
    let r, g, b;
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      const hue2rgb = (p, q, t) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }
    const toHex = x => {
      const hex = Math.round(x * 255).toString(16);
      return hex.length === 1 ? "0" + hex : hex;
    };
    return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
  }

  constructor(
    private http: HttpClient,
    private router: Router,
    private Listener: ListenerService,
    private parent: AppComponent
  ) {}

  ngOnInit() {
    // setTimeout(()=>{ this.loadEWS();},300);
    this.getMediaGroup().then(res => {
      this.loadEWS();
    });

    this.loadSocmed1().then(res => {
      let words = this.socmed1.data.words;
      let seriesData = words.map(o => {
        return { name: o[0], weight: o[1] };
      });
      this.report_val = true;
      seriesData.splice(0, 20);

      this.wordcloud = new Chart({
        chart: {
          backgroundColor: "transparent",
          height: 300,
          width: 300
        },
        plotOptions: {},
        series: [
          {
            type: "wordcloud",
            data: seriesData,
            name: "Occurrences"
          }
        ],
        title: {
          text: null
        }
      });

      this.loadSocmed2().then(res => {
        this.report_val = true;
      });
    });
    setInterval(() => {
      this.loadEWS();
      // this.updatefun();
    }, 60000);
  }
}
