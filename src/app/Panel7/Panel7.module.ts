import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Panel7Component } from "./Panel7.component";
import { Panel7RoutingModule } from "./Panel7-routing.module";

import { ChartsModule } from "ng2-charts";
import "chart.piecelabel.js";

//digivla module
import { CategoryModuleModule } from "../CategoryModule/CategoryModule.module";
import { MediaModuleModule } from "../MediaModule/MediaModule.module";
import { TimePickerModuleModule } from "../TimePickerModule/TimePickerModule.module";
import { ArticlesModuleModule } from "../ArticlesModule/ArticlesModule.module";

import { ChartModule, HIGHCHARTS_MODULES } from "angular-highcharts";
import * as more from "highcharts/highcharts-more.src";
import * as exporting from "highcharts/modules/exporting.src";
import * as offlineExporting from "highcharts/modules/offline-exporting.src";
import * as highstock from "highcharts/modules/stock.src";
import * as wordcloud from "highcharts/modules/wordcloud.src";
import { NgxGaugeModule } from "ngx-gauge";

@NgModule({
  imports: [
    CommonModule,
    Panel7RoutingModule,
    ChartsModule,
    CategoryModuleModule,
    MediaModuleModule,
    TimePickerModuleModule,
    ArticlesModuleModule,
    ChartModule,
    NgxGaugeModule
  ],
  declarations: [Panel7Component],
  providers: [
    {
      provide: HIGHCHARTS_MODULES,
      useFactory: () => [more, wordcloud, offlineExporting, highstock]
    } // add as factory to your providers
  ]
})
export class Panel7Module {}
