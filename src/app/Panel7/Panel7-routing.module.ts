import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Panel7Component } from './Panel7.component';



const routes: Routes = [
  { 
    path: '', 
    component: Panel7Component 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel7RoutingModule { }
