import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedsosModuleComponent } from './medsos-module.component';

describe('MedsosModuleComponent', () => {
  let component: MedsosModuleComponent;
  let fixture: ComponentFixture<MedsosModuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedsosModuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedsosModuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
