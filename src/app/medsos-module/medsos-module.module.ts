import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedsosModuleComponent } from './medsos-module.component';
import { ModalModule } from 'ngx-bootstrap/modal';

import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FormsModule } from '@angular/forms';
// import { ArticleDetailModuleModule } from '../ArticleDetailModule/ArticleDetailModule.module';


@NgModule({
  imports: [
    CommonModule,
    // ModalModule,
    FormsModule,    
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    // ArticleDetailModuleModule
  ],
  declarations: [MedsosModuleComponent],
  exports: [
    MedsosModuleComponent
  ]
})
export class medsosmoduleModule { }
