import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-medsos-module',
  templateUrl: './medsos-module.component.html',
  styleUrls: ['./medsos-module.component.scss']
})
export class MedsosModuleComponent implements OnInit {

  medsosList: any;
  currentPage = 0;
  pageSize:number=10;
  rcordTot

  value;
  tglAwal;
  tglAkhir;
  topic;

  date;
  sourceMedoso;
  urlMedosos;
  contentMedsos;
  titleMedsos;

  pointerArray;
  falagPage;
    
  @ViewChild('articlesModal') public articlesModal;
  @ViewChild('medsosDetailModal') public medsosDetailModal;
 

  constructor(private http: HttpClient,private parent: AppComponent) { }


  loadMedsos(value,tglAwal,tglAkhir,topic,flagPage){
    interface MedsosResponse {
      data: Object;
    }

    this.rcordTot = value;
    this.tglAwal = tglAwal;
    this.tglAkhir = tglAkhir;
    this.topic =topic;
    this.falagPage = flagPage;

    // let body = null
  //  let path= 'http://kemlu-v2.onlinemonitoring.id/API/getPosts?'
  let path= 'https://'+ this.parent.Wordcloud + '.onlinemonitoring.id/API/getPosts?'
    this.http.get<MedsosResponse>(path + 'tgl_awal=' + tglAwal + '&tgl_akhir=' + tglAkhir + '&page=' + this.currentPage + '&topic=' + topic)
    .subscribe((result: any) =>{
      this.medsosList = result;
      this.articlesModal.show();
    })

  }


  loadMedsosSentiment(value,tglAwal,tglAkhir,sentiment,flagPage){
    interface MedsosResponse {
      data: Object;
    }

    if (sentiment == 'Netral'){
      this.topic = 0;
    }else if(sentiment == 'Positif'){
      this.topic = 1;
    }else if(sentiment == 'Negatif'){
      this.topic = -1;
    }

    this.rcordTot = value;
    this.tglAwal = tglAwal;
    this.tglAkhir = tglAkhir;
    this.falagPage = flagPage;

    // let body = null
   
  //  let path= 'https://kemlu-v2.onlinemonitoring.id/API/getPosts?'
  let path= 'https://'+ this.parent.Wordcloud + '.onlinemonitoring.id/API/getPosts?'
    this.http.get<MedsosResponse>(path + 'tgl_awal=' + tglAwal + '&tgl_akhir=' + tglAkhir + '&page=' + this.currentPage + '&sentiment=' + this.topic)
    .subscribe((result: any) =>{
      this.medsosList = result;
      this.articlesModal.show();
    })

  }




  showArticleDetail(article){
    
    this.pointerArray = article;
    this.date = this.pointerArray.date;
    this.titleMedsos = this.pointerArray.title;
    this.contentMedsos = this.pointerArray.content;
    this.urlMedosos = this.pointerArray.url;
    this.sourceMedoso = this.pointerArray.source;
    // this.articlesModal.hide();
    this.medsosDetailModal.show();
    
  }


  pageChanged(event: any): void {    
    this.currentPage= event.page;
    
    // this.loadMedsos(this.rcordTot,this.tglAwal,this.tglAkhir,this.topic);
    if (this.falagPage == 'panel1'){
      this.loadMedsos(this.rcordTot,this.tglAwal,this.tglAkhir,this.topic,this.falagPage);

    }else if(this.falagPage == 'panel2'){
      this.loadMedsosSentiment(this.rcordTot,this.tglAwal,this.tglAkhir,this.topic,this.falagPage);
    }
  }

  ngOnInit() {
    // this.loadMedsos();
  }

}
