import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { Router } from "@angular/router";
import { ListenerService } from "../Service/Listener.service";
import { Subscription } from "rxjs";
import { AppComponent } from "../app.component";

@Component({
  selector: "app-Panel6",
  templateUrl: "./Panel6.component.html",
  styleUrls: ["./Panel6.component.scss"]
})
export class Panel6Component implements OnInit {
  private subscription: Subscription;

  //filter parameter
  today = new Date();
  periodFromModel: Date = this.addDays(this.today, -7);
  periodEndModel: Date = this.today;
  loaded = false;
  loaded2 = false;

  //fitlerparam
  category_set = "0";
  user_media_type_id = "0";
  timeFrame = 2;

  //bar Chart
  public barChartData: Array<any>;
  public barChartLabels: Array<any>;
  public barChartColors: Array<any>;
  public barChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: true,
      position: "bottom",
      labels: {
        fontColor: "whitesmoke"
      }
    },
    scales: {
      xAxes: [
        {
          stacked: false,
          gridLines: {
            display: true
          }
        }
      ],
      yAxes: [
        {
          stacked: false,
          gridLines: {
            drawBorder: false
          },
          ticks: {
            beginAtZero: true
          }
        }
      ]
    }
  };

  //doughnut chart
  public pieChartData: Array<any>;
  public pieChartLabels: Array<any>;
  public pieChartColors: Array<any>;
  public pieChartOptions: any = {
    responsive: true,
    maintainAspectRatio: false,
    pieceLabel: {
      fontColor: "whitesmoke",
      render: function(args) {
        const value = args.percentage;

        return value + "%";
      }
    },
    legend: {
      display: false
    }
  };

  @ViewChild("timePickerContainer") public timePickerContainer;
  @ViewChild("articlesContainer") public articlesContainer;

  public barChartLegend: boolean = true;
  public barChartType: string = "bar";
  public pieChartType: string = "doughnut";

  constructor(
    private http: HttpClient,
    private router: Router,
    private Listener: ListenerService,
    private parent: AppComponent
  ) {}

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }

  checkKemlu() {
    var domain = window.location.hostname.split(".")[1];
    if (this.parent.subdomain == "kemlu" && domain == "10.128.10.40") {
      interface UserResponse {
        data: Object;
      }
      this.http
        .get<UserResponse>(
          "http://10.128.10.40:3001/api/watson/bysentiment",
          this.parent.options
        )
        .subscribe(
          (result: any) => {
            // console.log(result);
            let barChartCoverageTones = [];
            let xAxisGraphBar = [];
            let lengthPositive = 0;
            let lengthNeutral = 0;
            let lengthNegative = 0;

            for (let i = 0; i < result.data.length; i++) {
              let dataSubCategory = [];
              let bgColor = [];
              for (
                let j = 0;
                j < result.data[i].tone_per_day.buckets.length;
                j++
              ) {
                dataSubCategory.push(
                  result.data[i].tone_per_day.buckets[j].doc_count
                );

                if (result.data[i].key == 1) {
                  lengthPositive = lengthPositive + 1;
                } else if (result.data[i].key == 0) {
                  lengthNeutral = lengthNeutral + 1;
                } else {
                  lengthNegative = lengthNegative + 1;
                }

                // bgColor.push(result.data[i].key==1?'rgba(255, 99, 132, 0.2)':(result.data[i].key==0?'rgba(255, 99, 132, 0.2)':'rgba(255, 99, 132, 0.2)'))
              }
              var index =
                result.data[i].key == 1 ? 0 : result.data[i].key == 0 ? 1 : 2;
              barChartCoverageTones.splice(index, 0, {
                data: dataSubCategory,
                label:
                  result.data[i].key == 1
                    ? "Positive"
                    : result.data[i].key == 0
                    ? "Neutral"
                    : "Negative"
                // 'color':bgColor
              });
            }

            for (
              let i = 0;
              i < result.data[0].tone_per_day.buckets.length;
              i++
            ) {
              xAxisGraphBar.push(
                result.data[0].tone_per_day.buckets[i].key_as_string
              );
            }

            this.barChartData = [];
            this.barChartData = barChartCoverageTones;

            this.barChartLabels = [];
            this.barChartLabels = xAxisGraphBar;
            let colorChart = [];
            if (lengthPositive > 0) {
              colorChart.push({ backgroundColor: "rgba(100, 132, 65,0.9)" });
            }
            if (lengthNeutral > 0) {
              colorChart.push({ backgroundColor: "rgba(1, 163, 212,0.9)" });
            }
            if (lengthNegative > 0) {
              colorChart.push({ backgroundColor: "rgba(162, 47, 54,0.9)" });
            }

            this.barChartColors = [];
            this.barChartColors = colorChart;

            setTimeout(() => {
              this.mouseEnter("time-frame-" + this.timeFrame);
            }, 1000);
            this.loaded = true;
          },
          (err: any) => {}
        );
    }
  }

  pieChartClicked(e: any, data) {
    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if (activePoints.length > 0) {
        // get the internal index of slice in pie chart
        const clickedElementIndex = activePoints[0]._index;
        const clickDatasetsIndex = activePoints[0]._datasetIndex;

        const tone =
          chart.data.labels[clickedElementIndex] == "Positive"
            ? 1
            : chart.data.labels[clickedElementIndex] == "Neutral"
            ? 0
            : -1;
        const start = moment(this.periodFromModel)
          .format("YYYY-MM-DD")
          .toString();
        const end = moment(this.periodEndModel)
          .format("YYYY-MM-DD")
          .toString();
        this.articlesContainer.loadArticles(
          this.category_set,
          this.user_media_type_id.toString(),
          data.media_id,
          "all",
          start,
          end,
          1,
          tone
        );
      }
    }
  }

  public chartClicked(e: any): void {
    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if (activePoints.length > 0) {
        // get the internal index of slice in pie chart
        const clickedElementIndex = activePoints[0]._index;
        const clickDatasetsIndex = activePoints[0]._datasetIndex;

        const label = moment(chart.data.labels[clickedElementIndex])
          .format("YYYY-MM-DD")
          .toString();

        const value =
          chart.data.datasets[clickDatasetsIndex].data[clickedElementIndex];
        const tone =
          chart.data.datasets[clickDatasetsIndex].label == "Positive"
            ? 1
            : chart.data.datasets[clickDatasetsIndex].label == "Neutral"
            ? 0
            : -1;
        let start = label;
        let end = label;
        if (this.timeFrame == 4) {
          var dateConverted = moment(label).toDate();
          dateConverted = this.addDays(dateConverted, 30);
          end = moment(dateConverted)
            .format("YYYY-MM-DD")
            .toString();
        }
        this.articlesContainer.loadArticles(
          this.category_set,
          this.user_media_type_id.toString(),
          0,
          "all",
          start,
          end,
          1,
          tone
        );
      }
    }
  }

  onKey(res: any) {
    
    if (res.code == "ArrowLeft") {
      this.router.navigateByUrl("/panel5");
    } else if (res.code == "ArrowRight") {
      this.router.navigateByUrl("/panel7");
    } else if (res.code == "ArrowDown") {
      this.router.navigateByUrl("/panelDesc");
    }
  }

  mouseEnter(div: string) {
    var className = document.getElementById(div).querySelectorAll("img");
    var re = new RegExp("white", "gi");
    className[0].src = className[0].src.replace(re, "primary");
  }

  mouseLeave(div: string) {
    // console.log(div.split('-')[2])
    if (div.split("-")[2] != this.timeFrame.toString()) {
      var className = document.getElementById(div).querySelectorAll("img");
      var re = new RegExp("primary", "gi");
      className[0].src = className[0].src.replace(re, "white");
    }
  }

  loadBarChart() {
    this.loaded = false;
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.category_set,
      category_id: "all",
      user_media_type_id: this.user_media_type_id,
      media_id: "0",
      start_date: moment(this.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/tones",
        editingParam,
        this.parent.options
      )
      .subscribe(
        (result: any) => {
          // console.log(result);
          let barChartCoverageTones = [];
          let xAxisGraphBar = [];
          let lengthPositive = 0;
          let lengthNeutral = 0;
          let lengthNegative = 0;
          for (let i = 0; i < result.data.chart_bar.length; i++) {
            let dataSubCategory = [];
            let bgColor = [];
            for (
              let j = 0;
              j < result.data.chart_bar[i].tone_per_day.buckets.length;
              j++
            ) {
              dataSubCategory.push(
                result.data.chart_bar[i].tone_per_day.buckets[j].doc_count
              );

              if (result.data.chart_bar[i].key == 1) {
                lengthPositive = lengthPositive + 1;
              } else if (result.data.chart_bar[i].key == 0) {
                lengthNeutral = lengthNeutral + 1;
              } else {
                lengthNegative = lengthNegative + 1;
              }

              // bgColor.push(result.data[i].key==1?'rgba(255, 99, 132, 0.2)':(result.data[i].key==0?'rgba(255, 99, 132, 0.2)':'rgba(255, 99, 132, 0.2)'))
            }
            var index =
              result.data.chart_bar[i].key == 1 ? 0 : result.data.chart_bar[i].key == 0 ? 1 : 2;
            barChartCoverageTones.splice(index, 0, {
              data: dataSubCategory,
              label:
                result.data.chart_bar[i].key == 1
                  ? "Positive"
                  : result.data.chart_bar[i].key == 0
                  ? "Neutral"
                  : "Negative"
              // 'color':bgColor
            });
          }

          for (let i = 0; i < result.data.chart_bar[0].tone_per_day.buckets.length; i++) {
            xAxisGraphBar.push(
              result.data.chart_bar[0].tone_per_day.buckets[i].key_as_string
            );
          }

          this.barChartData = [];
          this.barChartData = barChartCoverageTones;

          this.barChartLabels = [];
          this.barChartLabels = xAxisGraphBar;
          let colorChart = [];
          if (lengthPositive > 0) {
            colorChart.push({ backgroundColor: "rgba(100, 132, 65,0.9)" });
          }
          if (lengthNeutral > 0) {
            colorChart.push({ backgroundColor: "rgba(1, 163, 212,0.9)" });
          }
          if (lengthNegative > 0) {
            colorChart.push({ backgroundColor: "rgba(162, 47, 54,0.9)" });
          }

          this.barChartColors = [];
          this.barChartColors = colorChart;

          setTimeout(() => {
            this.mouseEnter("time-frame-" + this.timeFrame);
          }, 1000);
          this.loaded = true;
        },
        (err: any) => {}
      );
  }

  loadPieChart() {
    this.loaded2 = false;
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.category_set,
      category_id: "all",
      user_media_type_id: this.user_media_type_id,
      media_id: "0",
      start_date: moment(this.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/tone-by-media",
        editingParam,
        this.parent.options
      )
      .subscribe((result: any) => {
        // console.log(result);
        let rawData = [];
        for (let i = 0; i < result.data.length; i++) {
          let mediaData = [];
          let tones = [];
          let colors = [];
          for (let j = 0; j < result.data[i].tones.length; j++) {
            if (result.data[i].tones[j].positive != undefined) {
              mediaData.push(result.data[i].tones[j].positive);
              tones.push("Positive");
              colors.push("rgba(100, 132, 65,0.9)");
            } else if (result.data[i].tones[j].neutral != undefined) {
              mediaData.push(result.data[i].tones[j].neutral);
              tones.push("Neutral");
              colors.push("rgba(1, 163, 212,0.9)");
            } else {
              mediaData.push(result.data[i].tones[j].negative);
              tones.push("Negative");
              colors.push("rgba(162, 47, 54,0.9)");
            }
          }
          rawData.push({
            label: tones,
            data: mediaData,
            media: result.data[i].media_name,
            media_id: result.data[i].media_id,
            color: [{ backgroundColor: colors }]
          });
        }
        
        this.pieChartData = [];
        this.pieChartData = rawData.splice(0, 5); //[350, 450, 100];
        this.pieChartLabels = [];
        this.pieChartLabels = [
          "Download Sales",
          "In-Store Sales",
          "Mail-Order Sales"
        ];
        this.loaded2 = true;
      });
  }

  timeClick(time) {
    if (time == 1) {
      // console.log(this.addDays(this.today,-1));
      this.periodFromModel = this.addDays(this.today, -1);
      this.periodEndModel = this.today;
    } else if (time == 2) {
      this.periodFromModel = this.addDays(this.today, -7);
      this.periodEndModel = this.today;
    } else if (time == 3) {
      this.periodFromModel = this.addDays(this.today, -30);
      this.periodEndModel = this.today;
    } else if (time == 4) {
      this.periodFromModel = this.addDays(this.today, -360);
      this.periodEndModel = this.today;
    } else if (time == 5) {
      this.timePickerContainer.timePickerModal.show();
    }

    this.timeFrame = time;
    this.loadBarChart();
    this.loadPieChart();
  }

  ngOnInit() {
    this.loadBarChart();
    this.loadPieChart();
    setInterval(() => {
      this.loadBarChart();
      this.loadPieChart();
    }, 60000);

    this.subscription = this.Listener.notifyObservable$.subscribe(res => {
      if (res.hasOwnProperty("option") && res.option === "category") {
        // console
        this.category_set = res.value;
        this.loadBarChart();
        this.loadPieChart();
      } else if (res.hasOwnProperty("option") && res.option === "media") {
        // console
        this.user_media_type_id = res.value;
        
        this.loadBarChart();
        this.loadPieChart();
      } else if (res.hasOwnProperty("option") && res.option === "time_frame") {
        // console
        this.periodFromModel = res.value.start;
        this.periodEndModel = res.value.end;
        this.loadBarChart();
        this.loadPieChart();
      }
    });
  }
}
