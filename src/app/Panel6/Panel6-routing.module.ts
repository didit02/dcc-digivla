import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { Panel6Component } from "./Panel6.component";

const routes: Routes = [
  {
    path: "",
    component: Panel6Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel6RoutingModule {}
