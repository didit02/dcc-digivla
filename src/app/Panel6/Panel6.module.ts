import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Panel6Component } from './Panel6.component';
import { Panel6RoutingModule } from './Panel6-routing.module';


import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import 'chart.piecelabel.js';

//digivla module
import { CategoryModuleModule } from '../CategoryModule/CategoryModule.module'
import { MediaModuleModule } from '../MediaModule/MediaModule.module'
import { TimePickerModuleModule } from '../TimePickerModule/TimePickerModule.module'
import { ArticlesModuleModule } from '../ArticlesModule/ArticlesModule.module'


@NgModule({
    imports: [
        CommonModule,
        Panel6RoutingModule,
        ChartsModule,
        CategoryModuleModule,
        MediaModuleModule,
        TimePickerModuleModule,
        ArticlesModuleModule,
    ],
    declarations: [Panel6Component],
    providers: []
})
export class Panel6Module { }
