import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Panel9Component } from "./Panel9.component";
import { Panel9RoutingModule } from "./Panel9-routing.module";
import { ModalModule } from "ngx-bootstrap/modal";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { LeafletMarkerClusterModule } from "@asymmetrik/ngx-leaflet-markercluster";
import { HttpClientModule } from "@angular/common/http";
import { NgScrollbarModule } from "ngx-scrollbar";
import { ArticleDetailModuleModule } from "../ArticleDetailModule/ArticleDetailModule.module";

@NgModule({
  imports: [
    CommonModule,
    Panel9RoutingModule,
    LeafletModule.forRoot(),
    LeafletMarkerClusterModule,
    HttpClientModule,
    NgScrollbarModule,
    ArticleDetailModuleModule,
    ModalModule.forRoot()
  ],
  declarations: [Panel9Component]
})
export class Panel9Module {}
