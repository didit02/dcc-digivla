import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import * as L from "leaflet";
import * as lodash from "lodash";
// import { icon, latLng, Layer, Map, marker, point, polyline, tileLayer } from 'leaflet';

import "leaflet.markercluster";
import { AppComponent } from "../app.component";
import { ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { reject } from "q";

@Component({
  selector: "app-Panel9",
  templateUrl: "./Panel9.component.html",
  styleUrls: ["./Panel9.component.scss"]
})
export class Panel9Component implements OnInit {
  // _ = require('lodash');

  jabar_city = [
    {
      id: "3201",
      province_id: "32",
      name: "KABUPATEN BOGOR",
      alt_name: "KABUPATEN BOGOR",
      latitude: -6.58333,
      longitude: 106.71667
    },
    {
      id: "3202",
      province_id: "32",
      name: "KABUPATEN SUKABUMI",
      alt_name: "KABUPATEN SUKABUMI",
      latitude: -7.06667,
      longitude: 106.7
    },
    {
      id: "3203",
      province_id: "32",
      name: "KABUPATEN CIANJUR",
      alt_name: "KABUPATEN CIANJUR",
      latitude: -6.7725,
      longitude: 107.08306
    },
    {
      id: "3204",
      province_id: "32",
      name: "KABUPATEN BANDUNG",
      alt_name: "KABUPATEN BANDUNG",
      latitude: -7.1,
      longitude: 107.6
    },
    {
      id: "3205",
      province_id: "32",
      name: "KABUPATEN GARUT",
      alt_name: "KABUPATEN GARUT",
      latitude: -7.38333,
      longitude: 107.76667
    },
    {
      id: "3206",
      province_id: "32",
      name: "KABUPATEN TASIKMALAYA",
      alt_name: "KABUPATEN TASIKMALAYA",
      latitude: -7.5,
      longitude: 108.13333
    },
    {
      id: "3207",
      province_id: "32",
      name: "KABUPATEN CIAMIS",
      alt_name: "KABUPATEN CIAMIS",
      latitude: -7.28333,
      longitude: 108.41667
    },
    {
      id: "3208",
      province_id: "32",
      name: "KABUPATEN KUNINGAN",
      alt_name: "KABUPATEN KUNINGAN",
      latitude: -7,
      longitude: 108.55
    },
    {
      id: "3209",
      province_id: "32",
      name: "KABUPATEN CIREBON",
      alt_name: "KABUPATEN CIREBON",
      latitude: -6.8,
      longitude: 108.56667
    },
    {
      id: "3210",
      province_id: "32",
      name: "KABUPATEN MAJALENGKA",
      alt_name: "KABUPATEN MAJALENGKA",
      latitude: -6.81667,
      longitude: 108.28333
    },
    {
      id: "3211",
      province_id: "32",
      name: "KABUPATEN SUMEDANG",
      alt_name: "KABUPATEN SUMEDANG",
      latitude: -6.81667,
      longitude: 107.98333
    },
    {
      id: "3212",
      province_id: "32",
      name: "KABUPATEN INDRAMAYU",
      alt_name: "KABUPATEN INDRAMAYU",
      latitude: -6.45,
      longitude: 108.16667
    },
    {
      id: "3213",
      province_id: "32",
      name: "KABUPATEN SUBANG",
      alt_name: "KABUPATEN SUBANG",
      latitude: -6.50833,
      longitude: 107.7025
    },
    {
      id: "3214",
      province_id: "32",
      name: "KABUPATEN PURWAKARTA",
      alt_name: "KABUPATEN PURWAKARTA",
      latitude: -6.58333,
      longitude: 107.45
    },
    {
      id: "3215",
      province_id: "32",
      name: "KABUPATEN KARAWANG",
      alt_name: "KABUPATEN KARAWANG",
      latitude: -6.26667,
      longitude: 107.41667
    },
    {
      id: "3216",
      province_id: "32",
      name: "KABUPATEN BEKASI",
      alt_name: "KABUPATEN BEKASI",
      latitude: -6.24667,
      longitude: 107.10833
    },
    {
      id: "3217",
      province_id: "32",
      name: "KABUPATEN BANDUNG BARAT",
      alt_name: "KABUPATEN BANDUNG BARAT",
      latitude: -6.83333,
      longitude: 107.48333
    },
    {
      id: "3218",
      province_id: "32",
      name: "KABUPATEN PANGANDARAN",
      alt_name: "KABUPATEN PANGANDARAN",
      latitude: -7.6673,
      longitude: 108.64037
    },
    {
      id: "3271",
      province_id: "32",
      name: "KOTA BOGOR",
      alt_name: "KOTA BOGOR",
      latitude: -6.59167,
      longitude: 106.8
    },
    {
      id: "3272",
      province_id: "32",
      name: "KOTA SUKABUMI",
      alt_name: "KOTA SUKABUMI",
      latitude: -6.95,
      longitude: 106.93333
    },
    {
      id: "3273",
      province_id: "32",
      name: "KOTA BANDUNG",
      alt_name: "KOTA BANDUNG",
      latitude: -6.9175,
      longitude: 107.62444
    },
    {
      id: "3274",
      province_id: "32",
      name: "KOTA CIREBON",
      alt_name: "KOTA CIREBON",
      latitude: -6.75,
      longitude: 108.55
    },
    {
      id: "3275",
      province_id: "32",
      name: "KOTA BEKASI",
      alt_name: "KOTA BEKASI",
      latitude: -6.28333,
      longitude: 106.98333
    },
    {
      id: "3276",
      province_id: "32",
      name: "KOTA DEPOK",
      alt_name: "KOTA DEPOK",
      latitude: -6.4,
      longitude: 106.81667
    },
    {
      id: "3277",
      province_id: "32",
      name: "KOTA CIMAHI",
      alt_name: "KOTA CIMAHI",
      latitude: -6.89167,
      longitude: 107.55
    },
    {
      id: "3278",
      province_id: "32",
      name: "KOTA TASIKMALAYA",
      alt_name: "KOTA TASIKMALAYA",
      latitude: -7.35,
      longitude: 108.21667
    },
    {
      id: "3279",
      province_id: "32",
      name: "KOTA BANJAR",
      alt_name: "KOTA BANJAR",
      latitude: -7.36996,
      longitude: 108.53209
    }
  ];

  constructor(
    private http: HttpClient,
    private parent: AppComponent,
    private ref: ChangeDetectorRef,
    private router: Router
  ) {}
  @ViewChild("articleDetailContainer") public articleDetailContainer;
  public scrollbarOptions = { axis: "y", theme: "minimal-dark" };

  shown: "native" | "hover" | "always" = "native";
  detail: boolean = false;
  trendingList = [];
  myArray = [];
  article = [];
  //filter parameter
  today = new Date();
  periodFromModel: Date = this.addDays(this.today, -7);
  periodEndModel: Date = this.today;
  media_set = "0";
  url = this.parent.url;
  city = "Kota Bandung";
  geoTag = "Kota Bandung";
  total_article = Math.floor(Math.random() * 201) + 1;
  geoData: L.Layer[] = [];

  onKey(res: any) {
    console.log(res);
    if (res.code == "ArrowLeft") {
      this.router.navigateByUrl("/panel8");
    } else if (res.code == "ArrowDown") {
      this.router.navigateByUrl("/panelDesc");
    }
  }

  streetMaps = L.tileLayer(
    "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    {
      detectRetina: true,
      attribution:
        '&amp;copy; &lt;a href="https://www.openstreetmap.org/copyright"&gt;OpenStreetMap&lt;/a&gt; contributors'
    }
  );

  wMaps = L.tileLayer("http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png", {
    detectRetina: true,
    attribution:
      '&amp;copy; &lt;a href="https://www.openstreetmap.org/copyright"&gt;OpenStreetMap&lt;/a&gt; contributors'
  });

  layersControl = {
    baseLayers: {
      "Street Maps": this.streetMaps,
      "Wikimedia Maps": this.wMaps
    }
  };

  options = {
    layers: [this.streetMaps],
    zoom: 8,
    center: L.latLng([-6.4164539, 106.7457626])
  };

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }

  loadCategory() {
    interface UserResponse {
      data: Object;
    }
    //group category
    this.http
      .get<UserResponse>(this.url + "user/categories/", this.parent.options)
      .subscribe((result: any) => {
        this.myArray = result.results.map(o => {
          return o.category_set;
        });
      });
  }

  clipHandler(article) {
    // console.log(article);
    this.articleDetailContainer.loadArticle(article, true);
  }

  trendingHighlights(total) {
    interface UserResponse {
      data: Object;
    }

    let editingParam = {
      category_set: this.myArray[
        Math.floor(Math.random() * this.myArray.length)
      ],
      category_id: this.geoTag,
      user_media_type_id: "0",
      media_id: 0,
      start_date: moment(this.periodFromModel)
        .format("YYYY-MM-DD")
        .toString(),
      end_date: moment(this.periodEndModel)
        .format("YYYY-MM-DD")
        .toString()
    };

    this.http
      .post<UserResponse>(
        this.parent.url + "dashboard/high-lights",
        editingParam,
        this.parent.options
      )
      .subscribe((result: any) => {
        this.trendingList = result.data.slice(0, total);
        this.ref.detectChanges();
      });
  }

  ngOnInit() {
    this.loadCategory();
    this.generateData();
    this.trendingHighlights(10);
  }

  popup = L.popup();
  generateData() {
    const data: any[] = [];

    for (let i = 0; i < this.jabar_city.length; i++) {
      const icon = L.icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        iconUrl: "assets/img/leaflet/images/marker-icon.png",
        shadowUrl: "assets/img/leaflet/images/marker-shadow.png"
      });
      var self = this;
      var customPopup = "<p><h5>" + this.city + "<h5><p>";

      var customOptions = {
        maxWidth: "500",
        className: "custom"
      };

      this.geoData.push(
        L.marker([this.jabar_city[i].latitude, this.jabar_city[i].longitude], {
          icon,
          title: this.jabar_city[i].alt_name
        })
          .on("click", function(a: any) {
            self.onClick(a);
          })
          .bindPopup(this.popup)
      );
    }
  }

  toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  onClick(e) {
    console.log(e);
    this.city = e.sourceTarget.options.title;
    var administration_region =
      this.city.split(" ")[0].length > 4
        ? "Kab " + this.toTitleCase(this.city.split(" ")[1])
        : "Kota " + this.toTitleCase(this.city.split(" ")[1]);
    this.loadArticles(administration_region).then(res => {
      var htmlPopUp =
        "<p><h6>" +
        this.city +
        "</h6></p> <p> Total Article : " +
        this.total_article +
        "</p><p> Print Media : " +
        Math.floor(this.total_article * 0.3) +
        "</p><p> Online Media : " +
        Math.floor(this.total_article * 0.6) +
        "</p><p> Television Media : " +
        Math.floor(this.total_article * 0.1) +
        "</p>";
      this.popup.setContent(htmlPopUp);
    });

    // this.trendingHighlights(10);
    this.ref.detectChanges();
  }

  loadArticles(city) {
    return new Promise(resolve => {
      interface UserResponse {
        data: Object;
      }

      var today = new Date();
      let editingParam = {
        category_set: "0",
        category_id: city,
        user_media_type_id: "0",
        media_id: "0",
        term: "",
        start_date: moment(this.periodFromModel)
          .format("YYYY-MM-DD")
          .toString(),
        end_date: moment(this.periodEndModel)
          .format("YYYY-MM-DD")
          .toString(),
        maxSize: 100,
        page: 0
      };

      this.http
        .post<UserResponse>(
          this.parent.url + "user/editing/",
          editingParam,
          this.parent.options
        )
        .subscribe(
          (result: any) => {
            this.trendingList = result.data;
            this.total_article = result.recordsTotal;
            resolve(this.trendingList);
            // console.log(this.trendingList);
            this.ref.detectChanges();
          },
          (err: any) => {
            reject(err);
          }
        );
    });
  }
}
