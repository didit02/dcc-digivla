import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Panel9Component } from './Panel9.component';



const routes: Routes = [
  { 
    path: '', 
    component: Panel9Component 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel9RoutingModule { }
