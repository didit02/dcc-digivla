import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { ListenerService } from '../Service/Listener.service'
// import { Panel5Component } from '../Panel5/Panel5.component'
import { AppComponent } from '../app.component'
import { environment } from '../../environments/environment'

@Component({
  selector: 'app-ArticlesModule',
  templateUrl: './ArticlesModule.component.html',
  styleUrls: ['./ArticlesModule.component.scss']
})
export class ArticlesModuleComponent implements OnInit {

  @ViewChild('articlesModal') public articlesModal: ModalDirective;
  @ViewChild('articleDetailContainer') public articleDetailContainer;
  @ViewChild('articleDetailContainerMaps') public articleDetailContainerMaps;

  articlesList: any = [];
  pageSize: number = 10;
  currentPage: number = 1;

  //fitlerparam
  category_set;
  category_id;
  user_media_type_id;
  media;
  start;
  end;
  tone;
  timeFrame = 2;
  nameuse;
  pagecurrentuse;
  constructor(private http: HttpClient, private parent: AppComponent) { }

  showArticleDetail(article) {
    this.articleDetailContainer.loadArticle(article, false);
    this.articleDetailContainerMaps.loadArticlesmap(article, false);
  }

  pageChanged(event: any): void {
    this.currentPage = event.page;
    // this.loadArticles(this.category_set, this.user_media_type_id, this.media, this.category_id, this.start, this.end, event.page, this.tone);
    this.loadArticlesmaps(this.nameuse,this.start, this.end ,event.page)
  }


  loadArticles(category_set, user_media_type_id, media, category_id, start, end, page, tone) {
    interface UserResponse {
      data: Object;
    }

    this.category_set = category_set;
    this.category_id = category_id;
    this.user_media_type_id = user_media_type_id;
    this.media = media;
    this.start = start;
    this.end = end;
    this.tone = tone;

    let editingParam = {
      category_set: category_set,
      category_id: category_id,
      user_media_type_id: user_media_type_id,
      media_id: media,
      term: null,
      start_date: start,
      end_date: end,
      maxSize: 10,
      page: page - 1
    };

    let endpoint = 'user/editing/'
    if (tone != null) {
      editingParam['tone'] = tone;
      endpoint = 'dashboard/article-by-tone'
    }

    this.http.post<UserResponse>(environment.apiUrl + endpoint, editingParam, this.parent.options)
      .subscribe((result: any) => {
        this.articlesList = result;
        this.articlesModal.show();
      }, (err: any) => {

      });
  }

  loadArticlesmaps(namakota,start,end,page) {
    this.nameuse = namakota
    this.start = start;
    this.end = end;
    this.pagecurrentuse = page - 1 
    let body = {
      start_date: start,
      end_date: end,
      page: this.pagecurrentuse,
      max_size: 10,
      geo_loc: this.nameuse,
      type_location:"article",
      category_id:"all",
      category_set:"0"
    }
    interface UserResponse {
      data: Object;
    }
    this.http.post<UserResponse>('https://geo.digivla.id/api/v2/article-by-geo/',body, this.parent.options)
      .subscribe((result: any) => {
        this.articlesList = result;
        this.articlesModal.show();
      }, (err: any) => {

      });
  }
  ngOnInit() {
  }


}
