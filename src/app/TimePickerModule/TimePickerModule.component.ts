import { Component, OnInit,ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ListenerService } from '../Service/Listener.service'

@Component({
  selector: 'app-TimePickerModule',
  templateUrl: './TimePickerModule.component.html',
  styleUrls: ['./TimePickerModule.component.scss']
})
export class TimePickerModuleComponent implements OnInit {

  @ViewChild('timePickerModal') public timePickerModal: ModalDirective;
  bsConfig: Partial<BsDatepickerConfig>;

  today = new Date();
  periodFromModel :Date = this.addDays(this.today,-7);
  periodEndModel:Date = this.today;

  constructor(private Listener: ListenerService) { }

  ngOnInit() {
    this.bsConfig = Object.assign({}, { containerClass: 'theme-default' });    
  }

  submitDate(){
    this.Listener.notifyOther({option: 'time_frame', value: {start:this.periodFromModel,end:this.periodEndModel}});
    this.timePickerModal.hide();
  }

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }

}
