import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimePickerModuleComponent } from './TimePickerModule.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  imports: [
    CommonModule,
    ModalModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [TimePickerModuleComponent],
  exports:[TimePickerModuleComponent]
})
export class TimePickerModuleModule { }
