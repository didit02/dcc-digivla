import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'app-Panel2',
  templateUrl: './Panel2.component.html',
  styleUrls: ['./Panel2.component.scss']
})
export class Panel2Component implements OnInit {
  max: number = 200;
  
  dynamic: number =120;
  loaded:boolean=false;

  positiveBar;
  neutralBar;
  negativeBar;

  // lineChart
  public lineChartData:Array<any> 
  public lineChartLabels:Array<any> 
  public lineChartColors:Array<any>
  public lineChartOptions:any = {
    responsive: true,
    // bezierCurve: false,
    maintainAspectRatio: false,
    legend:{
      display:true,
      position:'bottom',
      labels:{
        fontColor:'whitesmoke'
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: true,          
        },
        ticks: {
          display:false
        }
      }],
      yAxes: [{
        gridLines: {
          drawBorder: false,          

        },
        ticks: {
          display:false
        }
      }]
    }
  };
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  //doughnut chart
  public pieChartData:Array<any> 
  public pieChartLabels:Array<any> 
  public pieChartColors:Array<any>
  public pieChartOptions:any = {
    responsive: true,    
    maintainAspectRatio: false,
    pieceLabel: {
      fontColor: 'whitesmoke',
      render: function (args) {        
        const value = args.percentage;        
        return value+'%';
      }
    },
    legend:{
      display:false,
    },
    tooltips: {
      callbacks: {
        label: function(tooltipItem, data) {
          //get the concerned dataset
          var dataset = data.datasets[tooltipItem.datasetIndex];
          //calculate the total of this data set
          var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
            return previousValue + currentValue;
          });
          //get the current items value
          var currentValue = dataset.data[tooltipItem.index];
          //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
          var percentage = Math.floor(((currentValue/total) * 100)+0.5);
    
          return percentage + "%";
        }
      }
    }
  };
  public pieChartType:string = 'doughnut';  

  @ViewChild('medsosContainer') public medsosContainer;
  constructor(private http: HttpClient,private router: Router,private parent: AppComponent) { }


      //event
      public chartClicked(e:any):void {
        if(e.active.length > 0){
          const chart = e.active[0]._chart;
          const activePoints = chart.getElementAtEvent(e.event);
          if (activePoints.length > 0){
            const clickedElementIndex = activePoints[0]._index;
            const clickDatasetsIndex = activePoints[0]._datasetIndex;
            const label = moment(chart.data.labels[clickedElementIndex]).format('YYYY-MM-DD').toString();
            const value = chart.data.datasets[clickDatasetsIndex].data[clickedElementIndex];
            const category = chart.data.datasets[clickDatasetsIndex].label;
            const flagPagenation = 'panel2';
            
    
            this.medsosContainer.loadMedsosSentiment(value,label,label,category,flagPagenation);
          
    
          }
        }
    
    
      }


  onKey(res:any){
    
    if (res.code == 'ArrowLeft'){
      this.router.navigateByUrl('/panel1');  
    }else 
    if (res.code == 'ArrowRight'){
      this.router.navigateByUrl('/panel3');  
    }else if(res.code == 'ArrowDown'){
      this.router.navigateByUrl('/panelDesc');  
    } 
  }

  setMyStyles(pgBar){
    let styles = {
      width:pgBar.percent
    };
    return styles;
  }

  getPostList(){
    this.loaded=false;
    interface UserResponse {
      data: Object;
    }

    const headers = new HttpHeaders({
      'access-token': this.parent.tokenuse,
      'project-id': this.parent.projectid
    });
    const options = { headers:headers };
    // this.http.get<UserResponse>('https://'+ this.parent.socmed + '.digivla.id/api/onlinemonitoring/' + this.parent.socmed)
    this.http.get<UserResponse>('https://'+ this.parent.socmed + '.onlinemonitoring.id/API/')
    // this.http.get<UserResponse>('https://api.onlinemonitoring.id/digivla/command-center' , options)
    .subscribe((result: any) => {                 
      let dataGraph=[];
      let xAxisGraph=[];
      let graphColor=[];

      let arrayBar = Object.keys(result.data_page2.top_issue.positif).map(function(key) {
        return result.data_page2.top_issue.positif[key];
      });
      this.positiveBar = Object.keys(result.data_page2.top_issue.positif).map(function(key) {
        return result.data_page2.top_issue.positif[key];
      }).splice(0,5);
      this.neutralBar = Object.keys(result.data_page2.top_issue.netral).map(function(key) {
        return result.data_page2.top_issue.netral[key];
      }).splice(0,5);
      this.negativeBar = Object.keys(result.data_page2.top_issue.negatif).map(function(key) {
        return result.data_page2.top_issue.negatif[key];
      }).splice(0,5);

      
      this.positiveBar = this.kabayan2our(this.positiveBar);
      this.neutralBar = this.kabayan2our(this.neutralBar);
      this.negativeBar = this.kabayan2our(this.negativeBar);
      
    
      this.positiveBar = this.countPercent(this.positiveBar);
      this.neutralBar = this.countPercent(this.neutralBar);
      this.negativeBar = this.countPercent(this.negativeBar);
      
      // console.log(result.data_page2.graph_2.data)
      let color = ['rgba(100, 132, 65,0.9)','rgba(1, 163, 212,0.9)','rgba(162, 47, 54,0.9)']
      for (let i =0;i < result.data_page2.graph_2.data.length;i++){
        dataGraph.push({data:result.data_page2.graph_2.data[i],label:result.data_page2.graph_2.series[i]})
        graphColor.push({
          borderColor: color[i],
          pointBorderColor: color[i],
          pointHoverBackgroundColor: color[i],
          pointHoverBorderColor: color[i],
        })
      }

      let pieRaw=[]
      const totalData = result.data_page2.positif+result.data_page2.netral+result.data_page2.negatif
      // for (let i=0;i<3;i++){
      pieRaw.push({label:['Positive',''],data:[result.data_page2.positif,(totalData-result.data_page2.positif)],color:[{backgroundColor:['rgba(100, 132, 65,0.9)','rgba(37, 44, 39,0.9)']}]})
      pieRaw.push({label:['Neutral',''],data:[result.data_page2.netral,(totalData-result.data_page2.netral)],color:[{backgroundColor:['rgba(1, 163, 212,0.9)','rgba(37, 44, 39,0.9)']}]})
      pieRaw.push({label:['Negative',''],data:[result.data_page2.negatif,(totalData-result.data_page2.negatif)],color:[{backgroundColor:['rgba(162, 47, 54,0.9)','rgba(37, 44, 39,0.9)']}]})
      // }

      
      this.pieChartData=pieRaw;

      this.lineChartData=[]
      this.lineChartData = dataGraph;      
      this.lineChartLabels=[]
      this.lineChartLabels = result.data_page2.graph_2.labels;
      this.lineChartColors=[]
      this.lineChartColors = graphColor;


      this.loaded=true;
    });  
  }

  countPercent(arrayParam){
    for (let j =0; j< arrayParam.data.length;j++){
      var a = (arrayParam.data[j].value/arrayParam.total) *100
      // console.log(a);
      arrayParam.data[j]['percent'] = a.toFixed();
    }
    return arrayParam;
  }

  kabayan2our(arrayParam){
    let newArray=[];
    let total=0;
    for (let i =0; i< arrayParam.length;i++){
      var output = Object.entries(arrayParam[i]).map(([key, value]) => ({key,value,i}));      
      var val = +output[0].value
      total = total + val;
      newArray.push(output[0]); 
    }    
    return {total:total,data:newArray};
  }

  ngOnInit() {
    this.getPostList();
    setInterval(()=>{
      this.getPostList();
    },20000);   
  }

}
