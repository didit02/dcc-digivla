import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Panel2Component } from './Panel2.component';
import { Panel2RoutingModule } from './Panel2-routing.module'
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import 'chart.piecelabel.js';

import { ChartsModule } from 'ng2-charts';
import{ medsosmoduleModule} from '../medsos-module/medsos-module.module'

@NgModule({
  imports: [
    CommonModule,
    Panel2RoutingModule,
    ProgressbarModule.forRoot(),
    ChartsModule,
    medsosmoduleModule
  ],
  declarations: [Panel2Component]
})
export class Panel2Module { }
