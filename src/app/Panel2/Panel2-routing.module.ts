import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Panel2Component } from './Panel2.component';



const routes: Routes = [
  { path: '', component: Panel2Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel2RoutingModule { }
