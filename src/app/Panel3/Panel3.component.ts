import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Chart } from 'angular-highcharts';
import * as highC from 'highcharts/js/highcharts.js';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { moment } from 'ngx-bootstrap/chronos/test/chain';

@Component({
  selector: 'app-Panel3',
  templateUrl: './Panel3.component.html',
  styleUrls: ['./Panel3.component.scss']
})
export class Panel3Component implements OnInit {
  
  data=[];
  posts=[];
  wordcloud = new Chart ({});  
  loaded = false;


  today = new Date();
  startDate:Date;
  endDate:Date;
  objStartDate;
  objEnddate;

  constructor(private http: HttpClient,private router: Router,private parent: AppComponent) { }


  onKey(res:any){
    
    if (res.code == 'ArrowLeft'){
      this.router.navigateByUrl('/panel2');  
    }else 
    if (res.code == 'ArrowRight'){
      this.router.navigateByUrl('/panel4');  
    }else if(res.code == 'ArrowDown'){
      this.router.navigateByUrl('/panelDesc');  
    } 
  }

  private getWordCloudChart() {
    this.loaded=false;
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>('https://'+ this.parent.socmed + '.digivla.id/api/wordcloud/' + this.parent.subdomain)
    .subscribe((result: any) => {           
      let words = result.data.data.words;
      let seriesData= words.map(o => {
        return { name: o[0], weight: o[1] };
      });
    
      seriesData =  seriesData.splice(0,90);
      
      this.wordcloud = new Chart({
        chart: {
          backgroundColor:'transparent'             
        },
        plotOptions:{
          
        },
        series: [{
          type: 'wordcloud',
          data: seriesData,        
          name: 'Occurrences',                           
        }],
        title: {
          text: null
        }
      });  
      this.loaded=true;
    });  
  }


  getWorldCloudChartKabayan(){
    interface WorldCloudResponse {
      data: Object;
    }

    this.objStartDate =(moment(this.startDate).format('YYYY-MM-DD')).toString();
    this.objEnddate =(moment(this.endDate).format('YYYY-MM-DD')).toString();
    

    
    let path = 'https://'+ this.parent.Wordcloud + '.onlinemonitoring.id/API/getWordcloud?&tgl_awal='+this.objStartDate+'&tgl_akhir='+ this.objEnddate;
    // let path = 'http://kemlu-v2.onlinemonitoring.id/API/getWordcloud?&tgl_awal='+this.objStartDate+'&tgl_akhir='+ this.objEnddate;
    // let path = 'https://api.onlinemonitoring.id/digivla/command-center/getWordcloud?&tgl_awal='+this.objStartDate+'&tgl_akhir='+ this.objEnddate;
    // const headers = new HttpHeaders({
    //   'access-token': this.parent.tokenuse,
    //   'project-id': this.parent.projectid
    // });
    // const options = { headers:headers };
    // this.http.get<UserResponse>('https://'+ this.parent.socmed + '.digivla.id/api/onlinemonitoring/' + this.parent.socmed)
    // this.http.get<UserResponse>('https://'+ this.parent.socmed + '.onlinemonitoring.id/API/')
    // this.http.get<WorldCloudResponse>('https://api.onlinemonitoring.id/digivla/command-center' , options)
    this.http.get<WorldCloudResponse>(path)
    .subscribe((result: any) =>{
      
      let words = result;
      // const data =[    {
      //         name:"digivla",
      //         weight:500
      //     },
      //     {
      //         name:"kabayan",
      //         weight:20000
      //     },
      //   ]

      words.splice(0,20);
      // console.log(words);
      this.wordcloud = new Chart({
        chart: {
          backgroundColor:'transparent'             
        },
        plotOptions:{
          
        },
        series: [{
          type: 'wordcloud',
          data: words,        
          name: 'Occurrences',                           
        }],
        title: {
          text: null
        }
      });
      this.loaded=true;  

    });
  }



  getPostList(){
    interface UserResponse {
      data: Object;
    }

    var fd = {
      clientid:this.parent.socmed
    }
    this.http.get<UserResponse>('https://'+ this.parent.socmed + '.onlinemonitoring.id/API/')
    .subscribe((result: any) => {                 
      this.posts = Object.keys(result.data_page2.top_posts).map(function(key) {
        return result.data_page2.top_posts[key];
      });
      for (let i=0;i<this.posts.length;i++){
        var urlsource = this.posts[i].source;
        urlsource = urlsource.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0];
        this.posts[i]['socmed'] = urlsource
        this.posts[i].sentiment = this.posts[i].sentiment==1?'Positive':(this.posts[i].sentiment==0?'Neutral':'Negative')
      }
      
    });  
  }


  archimedeanSpiral(t) {
    t *= 0.1;
    return {
        x: t * Math.cos(t),
        y: t * Math.sin(t)
    };
  };


  addDays(date, days) {
    var result = new Date(date); 
    result.setDate(date.getDate() + days);
    return result;
  }

  Weekly(){
    this.startDate = this.addDays(this.today,-7);
    this.endDate= this.today;
    this.getWorldCloudChartKabayan(); 
  }


  ngOnInit() {
    // this.getWordCloudChart();
    this.Weekly();   
    this.getPostList();

    setInterval(()=>{
      // this.getWordCloudChart();
      this.Weekly();
      this.getPostList();
    },50000);   
  }

}
