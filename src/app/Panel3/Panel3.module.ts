import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Panel3Component } from './Panel3.component';
import { Panel3RoutingModule } from './Panel3-routing.module';

import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import * as offlineExporting from 'highcharts/modules/offline-exporting.src';
import * as highstock from 'highcharts/modules/stock.src';
import * as wordcloud from 'highcharts/modules/wordcloud.src';
import { NgScrollbarModule } from 'ngx-scrollbar';

@NgModule({
  imports: [
    CommonModule,
    ChartModule,
    Panel3RoutingModule,
    NgScrollbarModule
  ],
  declarations: [Panel3Component],
  providers: [
    { provide: HIGHCHARTS_MODULES, useFactory: () => [ more,wordcloud, offlineExporting,highstock ] } // add as factory to your providers
  ]
})
export class Panel3Module { }
