import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Panel8Component } from './Panel8.component';
import { Panel8RoutingModule } from './Panel8-routing.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgScrollbarModule } from 'ngx-scrollbar';

//digivla module
import { CategoryModuleModule } from '../CategoryModule/CategoryModule.module'
import { MediaModuleModule } from '../MediaModule/MediaModule.module'
import { TimePickerModuleModule } from '../TimePickerModule/TimePickerModule.module'
import { ArticlesModuleModule } from '../ArticlesModule/ArticlesModule.module'
@NgModule({
  imports: [
    CommonModule,
    Panel8RoutingModule,
    CategoryModuleModule,
    MediaModuleModule,
    TimePickerModuleModule,
    ArticlesModuleModule,
    PdfViewerModule,
    NgScrollbarModule
  ],
  declarations: [Panel8Component]
})
export class Panel8Module { }
