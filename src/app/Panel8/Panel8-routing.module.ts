import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Panel8Component } from './Panel8.component';



const routes: Routes = [
  { 
    path: '', 
    component: Panel8Component 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel8RoutingModule { }
