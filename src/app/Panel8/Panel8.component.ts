import { Component,OnInit,ViewChild} from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { Router } from '@angular/router';
import { ListenerService } from '../Service/Listener.service'
import { Subscription } from 'rxjs';
import { AppComponent } from '../app.component'
import { environment } from '../../environments/environment'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';


@Component({
  selector: 'app-Panel8',
  templateUrl: './Panel8.component.html',
  styleUrls: ['./Panel8.component.scss']
})
export class Panel8Component implements OnInit {
  url=this.parent.url;
  today = new Date();
  periodFromModel :Date = this.addDays(this.today,-7);
  periodEndModel:Date = this.today;
  loaded=false;
  urlFileModel="";
  mediaExtensionModel="";
  articles=[];
  listtv = [];
  listpdf = [];
  pdfurlwrap:any;
  loadpdf:boolean = false;
  category_set='0';
  user_media_type_id='0';
  user_media_type_id_tv='0';
  timeFrame = 2;  
  public innerHtml: SafeHtml;
  iframe:any;
 
  private subscription: Subscription;
  constructor(private http: HttpClient,private router: Router, private sanitizer:DomSanitizer,private Listener: ListenerService,private parent: AppComponent) { }
  @ViewChild('timePickerContainer') public timePickerContainer;

  onKey(res:any){
    if (res.code == 'ArrowLeft'){
      this.router.navigateByUrl('/panel7');  
    }else if( res.code == 'ArrowRight' && this.parent.subdomain=='jabar'){
      this.router.navigateByUrl('/panel9');
    }else if(res.code == 'ArrowDown'){
      this.router.navigateByUrl('/panelDesc');  
    } 
  }

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }

  loadCetak(){
    interface UserResponse {
      data: Object;
    } 
    let editingParamtv={
      "category_set": this.category_set,
      "category_id":'all',
      "user_media_type_id": this.parent.tv,
      "media_id": '0',
      "term": null,
      "start_date": (moment(this.periodFromModel).format('YYYY-MM-DD')).toString(),
      "end_date": (moment(this.periodEndModel).format('YYYY-MM-DD')).toString(),
      "maxSize": 20,
      "page": 0,
      "order_by": 'datee',
      'order':'desc'
    };
    let editingParampdf={
      "category_set": this.category_set,
      "category_id":'all',
      "user_media_type_id": this.parent.cetak,
      "media_id": '0',
      "term": null,
      "start_date": (moment(this.periodFromModel).format('YYYY-MM-DD')).toString(),
      "end_date": (moment(this.periodEndModel).format('YYYY-MM-DD')).toString(),
      "maxSize": 20,
      "page": 0,
      "order_by": 'datee',
      'order':'desc'
    };
    this.http.post<UserResponse>(environment.apiUrl +'user/editing/',editingParampdf,this.parent.options)
    .subscribe((result: any) => {         
      this.listpdf = result.data;
    },(err:any)=>{
    }); 
    this.http.post<UserResponse>(environment.apiUrl +'user/editing/',editingParamtv,this.parent.options)
    .subscribe((result: any) => {         
      this.listtv = result.data;
    },(err:any)=>{
    }); 
  }

  getMediaGroup() {
    return new Promise((resolve, reject) => {
      interface UserResponse {
        data: Object;
      }
      this.http.get<UserResponse>(this.url + 'user/medias/',this.parent.options)
      .subscribe((result: any) => {
        for (let i = 0; i < result.results.length;i++){
          var str = result.results[i].user_media_type_name_def;
          if ((str.toLowerCase()).includes('televisi') || (str.toLowerCase()).includes('tv')){
            this.user_media_type_id_tv = result.results[i].user_media_type_id;
            break;
          }
        }
        resolve(result);
      });
    })
  }

  loadTV(){
    interface UserResponse {
      data: Object;
    }

    
    let editingParam={
      "category_set": '0',
      "category_id":'all',
      "user_media_type_id":this.parent.tv,
      "media_id": '0',
      "term": null,
      "start_date": (moment(this.periodFromModel).format('YYYY-MM-DD')).toString(),
      "end_date": (moment(this.periodEndModel).format('YYYY-MM-DD')).toString(),
      "maxSize": 1,
      "page": 0,
      "order_by": 'datee',
      'order':'desc'
    };
    
    this.http.post<UserResponse>(environment.apiUrl  +'user/editing/',editingParam,this.parent.options)
    .subscribe((result: any) => {   
      this.mediaExtensionModel = result.data[0].file_pdf.split('.')[1];      
      this.urlFileModel = "https://input.digivla.id/media_tv/"+ result.data[0].file_pdf.split('-')[0] + "/" + result.data[0].file_pdf.split('-')[1] + "/" + result.data[0].file_pdf.split('-')[2] + "/" + result.data[0].file_pdf; 
    },(err:any)=>{

    }); 
  }

  clipHandler(article){
    this.mediaExtensionModel = article.file_pdf.split('.')[1];
    this.urlFileModel = (this.mediaExtensionModel=='pdf'?'https://pdf.digivla.id/pdf_images/':"https://input.digivla.id/media_tv/") + article.file_pdf.split('-')[0] + "/" + article.file_pdf.split('-')[1] + "/" + article.file_pdf.split('-')[2] + "/" + article.file_pdf;
    // this.urlFileModel = (this.mediaExtensionModel=='pdf'?'/apipdf/pdf_images/':"https://input.digivla.id/media_tv/") + article.file_pdf.split('-')[0] + "/" + article.file_pdf.split('-')[1] + "/" + article.file_pdf.split('-')[2] + "/" + article.file_pdf;
    this.innerHtml = this.sanitizer.bypassSecurityTrustResourceUrl('https://docs.google.com/viewer?url='+this.urlFileModel+'&embedded=true')    
    
  }



  timeClick(time) {
    if(time == 1){      
      // console.log(this.addDays(this.today,-1));
      this.periodFromModel = this.addDays(this.today,-1);
      this.periodEndModel = this.today; 
      
    }else if(time==2){
      this.periodFromModel = this.addDays(this.today,-7);
      this.periodEndModel = this.today; 
      
    }else if(time==3){
      this.periodFromModel = this.addDays(this.today,-30);
      this.periodEndModel = this.today;
      
    }else if(time==4){
      this.periodFromModel = this.addDays(this.today,-360);
      this.periodEndModel = this.today;
      
    }else if(time==5){
      this.timePickerContainer.timePickerModal.show();
    }
    
    this.timeFrame=time;
    this.loadCetak();
  }

  mouseEnter(div : string){
    var className = document.getElementById(div).querySelectorAll("img");
    var re = new RegExp('white', 'gi');     
    className[0].src = className[0].src.replace(re,'primary');
  }

  mouseLeave(div : string){
    if(div.split('-')[2]!=this.timeFrame.toString()){
      var className = document.getElementById(div).querySelectorAll("img");
      var re = new RegExp('primary', 'gi');     
      className[0].src = className[0].src.replace(re,'white');
    }    
  }


  ngOnInit() {    
    sessionStorage.setItem(
      "currentUser",
      JSON.stringify({
        socmed: this.parent.socmed,
      })
    );
    this.getMediaGroup().then((res)=>{
      this.loadTV();
    })

    this.loadCetak();
    setInterval(()=>{
      console.log('load');
      
      this.loadTV();
      this.loadCetak();
        },300000);

    this.subscription = this.Listener.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'category') {    
        console    
        this.category_set = res.value;
        this.loadCetak();
      } else if (res.hasOwnProperty('option') && res.option === 'media') {    
        console    
        this.user_media_type_id = res.value;
        this.loadCetak();
      } else if (res.hasOwnProperty('option') && res.option === 'time_frame') {    
        console    
        this.periodFromModel = res.value.start;
        this.periodEndModel = res.value.end;
        this.loadCetak();
      } 
    });
  }

}
