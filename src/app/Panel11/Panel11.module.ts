import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Panel11RoutingModule } from "./Panel11-routing.module";
import { Panel11Component } from "./Panel11.component";

//digivla module
import { CategoryModuleModule } from "../CategoryModule/CategoryModule.module";
import { MediaModuleModule } from "../MediaModule/MediaModule.module";
import { TimePickerModuleModule } from "../TimePickerModule/TimePickerModule.module";
import { ArticlesModuleModule } from "../ArticlesModule/ArticlesModule.module";

import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import * as highmaps from 'highcharts/modules/map.src';
@NgModule({
  imports: [
    CommonModule,
    Panel11RoutingModule,
    CategoryModuleModule,
    MediaModuleModule,
    TimePickerModuleModule,
    ArticlesModuleModule,
    ChartModule
  ],
  declarations: [Panel11Component],
  providers : [{ provide: HIGHCHARTS_MODULES, useFactory: () => [highmaps] }],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class Panel11Module {}
