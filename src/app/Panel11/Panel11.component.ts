import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { MapChart } from 'angular-highcharts';
import { ListenerService } from "../Service/Listener.service";
import { moment } from "ngx-bootstrap/chronos/test/chain";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";
import { AppComponent } from "../app.component";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4geodata_idAlbersHigh from "@amcharts/amcharts4-geodata/indonesiaHigh";
am4core.useTheme(am4themes_animated);
@Component({
  selector: "app-Panel11",
  templateUrl: "./Panel11.component.html",
  styleUrls: ["./Panel11.component.scss"]
})
export class Panel11Component implements OnInit {
  mapChart: any;
  options: any;
  customSeries: any;
  seriescolor: any;
  mincolor: string = '#D3D3D3';
  maxcolor: string = '#BFFFB2';
  subscription: Subscription;
  today = new Date();
  looppiechart = [];
  chartpie:any;
  periodFromModel: Date = this.addDays(this.today, -7);
  periodEndModel: Date = this.today;
  constructor(
    private statelocal: ListenerService,
    private http: HttpClient,
    private parent: AppComponent
  ) { }
  @ViewChild("articlesContainer") public articlesContainer;
  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }
  ngOnInit() {
    this.renderdata()
    // for (let i = 0 ; i <=3; i++){
    //   this.looppiechart.push(i)
    // }
    this.chartpie = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                }
            },
            startAngle: 100,
            endAngle: 100,
            center: ['50%', '50%'],
            size: '100%'
        }
    },
    series: [{
        type: 'pie',
        innerSize: '50%',
        data: [
            ['Chrome', 10],
            ['Firefox', 50],
            ['Firefox', 40]
        ]
    }]
    }
  }
  renderdata() {      
    this.statelocal.getlocalgeojson().subscribe(data => {
      interface UserResponse {
        data: Object;
      }
      let dataallloc = [];
      let body = {
        start_date: (moment(this.periodFromModel).format('YYYY-MM-DD')).toString(),
        end_date: (moment(this.periodEndModel).format('YYYY-MM-DD')).toString(),
        type_location: 'article',
        category_id: 'all',
        category_set: '0',
      }
      this.http.post<UserResponse>("https://geo.digivla.id/api/v2/all-count/" , body , this.parent.options)
        .subscribe(
          (result: any) => {
            result.data.forEach(element => {
              dataallloc.push([element.value, element.key.toUpperCase()])
            });
            let interval = 0;
            this.customSeries = []
            dataallloc.forEach((item) => {
              if (item[0] > 0) {
                this.seriescolor = this.calculateDark(this.maxcolor, interval);
              } else {
                this.seriescolor = this.mincolor;
              }
              this.customSeries.push({
                name: item[1],
                percent: item[0],
                color: this.seriescolor,
                data: [{ 'value': item[0], 'name': item[1] }],
                borderColor: '#212121',
                states: {
                  hover: {
                    color: '#52DE97',
                  },
                },
              })
              interval += 0.2;
            })

            this.options = {
              chart: {
                backgroundColor: 'rgba(255, 255, 255, 0.0)',
                height: 300
              },
              title: {
                text: ''
              },
              legend: {
                enabled: false,
                symbolRadius: 0,
                align: 'right',
                layout: 'vertical',
                verticalAlign: 'top',
                x: 0,
                y: 0,
                itemStyle: {
                  color: '#c9c9ca',
                },
                itemHoverStyle: {
                  color: '#FFFFFF',
                },
                itemHiddenStyle: {
                  color: '#414144',
                },
                useHTML: true,
                navigation: {
                  activeColor: '#cccccc',
                  inactiveColor: '#737373',
                  style: {
                    color: '#4a4a4a',
                    fontSize: '15px',
                  },
                  buttonOptions: {
                    enabled: false
                    }
                },
              },
              plotOptions: {
                map: {
                  allAreas: false,
                  joinBy: ['name'],
                  mapData: data,
                  tooltip: {
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{point.value}</b>'
                  }

                },
                series:{
                  point:{
                      events:{
                        click: (event)=>{
                        	this.onclickmaps(event);
                        }
                        }
                    }
                }
              },
              credits: {
                enabled: false
              },
              series: this.customSeries
            }
            this.mapChart = new MapChart(this.options); 
          },
          err => { 
          }
        );
    })
  }
  LightenDarkenColor(col, amt) {
    this.calculateDark(col, amt)
  }
  calculateDark(col, amt) {
    var usePound = false;
    if (col[0] == '#') {
      col = col.slice(1);
      usePound = true;
    }
    var num = parseInt(col, 16);
    var r = (num >> 16) - amt;
    if (r > 255) r = 255;
    else if (r < 0) r = 0;
    var b = (num >> 8) & 0x00ff;
    if (b > 255) b = 255;
    else if (b < 0) b = 0;
    var g = num & 0x0000ff;
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
    return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
  }
  onclickmaps(event){
    let namesplitkab;
    const start = moment(this.periodFromModel)
          .format("YYYY-MM-DD")
          .toString();
        const end = moment(this.periodEndModel)
          .format("YYYY-MM-DD")
          .toString();     
            namesplitkab = event.point.name
    this.articlesContainer.loadArticlesmaps(namesplitkab , start, end, 1);
  }
}
  