import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { Panel11Component } from "./Panel11.component";

const routes: Routes = [
  {
    path: "",
    component: Panel11Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel11RoutingModule {}
