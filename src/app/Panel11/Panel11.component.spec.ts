import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { Panel11Component } from "./Panel11.component";

describe("Panel11Component", () => {
  let component: Panel11Component;
  let fixture: ComponentFixture<Panel11Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Panel11Component]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Panel11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
