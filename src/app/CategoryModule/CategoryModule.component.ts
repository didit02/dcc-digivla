import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment'
import { ListenerService } from '../Service/Listener.service'
// import { Panel5Component } from '../Panel5/Panel5.component'
import { AppComponent } from '../app.component'

@Component({
  selector: 'app-CategoryModule',
  templateUrl: './CategoryModule.component.html',
  styleUrls: ['./CategoryModule.component.scss']
})
export class CategoryModuleComponent implements OnInit {
  groupCategoryOption;
  categorySelected;
  elementSelected='category-1';

  url=environment.apiUrl;

  constructor(private http: HttpClient,private Listener: ListenerService, private parent: AppComponent) { }

  mouseEnter(div : string){
    var className = document.getElementById(div);    
    className.style.color = '#52C6D8';
  }

  mouseLeave(div : string){
    if(div != this.elementSelected){
      var className = document.getElementById(div);
      className.style.color = 'whitesmoke';  
    }
  }

  categoryClick(cat){
    var className = document.getElementById('category-'+cat.iter);    
    className.style.color = '#52C6D8';
    
    if (this.elementSelected != 'category-'+cat.iter){
      document.getElementById(this.elementSelected).style.color = 'whitesmoke';
      this.Listener.notifyOther({option: 'category', value: cat.category_set});
    }
    this.elementSelected = 'category-'+cat.iter;
    this.categorySelected = cat.category_set;
    
  }

  getGroupCategory(){
    interface UserResponse {
      data: Object;
    }
    //group category
    this.http.get<UserResponse>(this.url + 'user/categories/',this.parent.options)
      .subscribe((result: any) => {        
        let totalRow = this.roundUp(result.results.slice(0,9).length/3,0);   
        var res = result.results.splice(0,9);
        for (let i =0;i<res.length;i++){
          res[i].iter=i+1;
        }
        var categoryPerRow=[]
        for (let i = 0; i < totalRow;i++){
            categoryPerRow.push({data:res.splice(0,3)});
        }  
        this.groupCategoryOption = categoryPerRow;  
        setTimeout(()=>{ this.categoryClick(this.groupCategoryOption[0].data[0])},1000);
      });
  }

  roundUp(num, precision) {
    precision = Math.pow(10, precision)
    return  Math.ceil(num * precision) / precision
  }

  ngOnInit() {
    // setTimeout(()=>{ 
    this.getGroupCategory();    
    // },1200);  
    
    
  }

}
