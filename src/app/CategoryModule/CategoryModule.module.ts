import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryModuleComponent } from './CategoryModule.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CategoryModuleComponent],
  exports:[CategoryModuleComponent]
})
export class CategoryModuleModule { }
