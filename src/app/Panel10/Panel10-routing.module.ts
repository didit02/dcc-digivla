import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { Panel10Component } from "./Panel10.component";

const routes: Routes = [
  {
    path: "",
    component: Panel10Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel10RoutingModule {}
