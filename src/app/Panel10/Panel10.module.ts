import { NgModule , NO_ERRORS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Panel10RoutingModule } from "./Panel10-routing.module";
import { Panel10Component } from "./Panel10.component";

import { ChartsModule } from "ng2-charts";
import "chart.piecelabel.js";

//digivla module
import { CategoryModuleModule } from "../CategoryModule/CategoryModule.module";
import { MediaModuleModule } from "../MediaModule/MediaModule.module";
import { TimePickerModuleModule } from "../TimePickerModule/TimePickerModule.module";
import { ArticlesModuleModule } from "../ArticlesModule/ArticlesModule.module";

import { ChartModule, HIGHCHARTS_MODULES } from "angular-highcharts";
import * as more from "highcharts/highcharts-more.src";
import * as exporting from "highcharts/modules/exporting.src";
import * as offlineExporting from "highcharts/modules/offline-exporting.src";
import * as highstock from "highcharts/modules/stock.src";
import * as wordcloud from "highcharts/modules/wordcloud.src";
import { NgxGaugeModule } from "ngx-gauge";
import { NgScrollbarModule } from "ngx-scrollbar";
@NgModule({
  imports: [
    CommonModule,
    Panel10RoutingModule,
    ChartsModule,
    CategoryModuleModule,
    MediaModuleModule,
    TimePickerModuleModule,
    ArticlesModuleModule,
    ChartModule,
    NgxGaugeModule,
    NgScrollbarModule
  ],
  declarations: [Panel10Component],
  providers: [
    {
      provide: HIGHCHARTS_MODULES,
      useFactory: () => [more, wordcloud, offlineExporting, highstock]
    } // add as factory to your providers
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class Panel10Module {}
