import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  NgZone
} from "@angular/core";
import * as $ from "jquery";
import { AppComponent } from "../app.component";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { HttpClient, HttpHeaders } from "@angular/common/http";

am4core.useTheme(am4themes_animated);
@Component({
  selector: "app-Panel10",
  templateUrl: "./Panel10.component.html",
  styleUrls: ["./Panel10.component.scss"]
})
export class Panel10Component implements OnInit {
  oripost: any;
  childerpost = [];
  datagroupall = [];
  pushdataall = [];
  datause = {};
  dataalluse = [];
  contenttooltip: string;
  hovernode: boolean;
  datadropdown: any;
  topicgetdata: any;
  loading:boolean = false;
  constructor(
    private zone: NgZone,
    private http: HttpClient,
    private parent: AppComponent
  ) {}
  ngAfterViewInit() {}
  ngOnInit() {
    this.loading = true;
    interface UserResponse {
      data: Object;
    }

    this.http
      .get<UserResponse>(
        "https://" +
          this.parent.socmed +
          ".onlinemonitoring.id/API/" 
      )
      .subscribe((result: any) => {
        this.datadropdown = Object.keys(
          result.data_page1.keyword_breakdown
        );
        this.topicgetdata = this.datadropdown[0];
        this.hovernode = false;
        this.contenttooltip = "";
        const chart = am4core.create(
          "sna",
          am4plugins_forceDirected.ForceDirectedTree
        );

        const networkSeries = chart.series.push(
          new am4plugins_forceDirected.ForceDirectedSeries()
        );
        networkSeries.dataFields.linkWith = "linkWith";
        networkSeries.dataFields.name = "name";
        networkSeries.dataFields.id = "name";
        networkSeries.dataFields.value = "value";
        networkSeries.dataFields.children = "children";

        networkSeries.nodes.template.label.text = "{name}";
        networkSeries.fontSize = 10;
        networkSeries.links.template.strength = 0.3;
        networkSeries.defaultState.transitionDuration = 0;
        networkSeries.centerStrength = 1.8;
        networkSeries.draggable = false;

        networkSeries.align = "center";

        networkSeries.manyBodyStrength = -6.8;
        networkSeries.minRadius = 13;
        networkSeries.maxRadius = 55;

        const nodeTemplate = networkSeries.nodes.template;
        nodeTemplate.tooltipText = "{name}";
        nodeTemplate.width = 1;
        nodeTemplate.fillOpacity = 1;
        nodeTemplate.label.hideOversized = false;
        nodeTemplate.label.truncate = false;

        const linkTemplate = networkSeries.links.template;
        linkTemplate.strokeWidth = 1;
        nodeTemplate.events.on("over", event => {
          const dataItem = event.target.dataItem;
          this.hovernode = true;
          this.contenttooltip = dataItem["_dataContext"]["data"];
          event.target.dataItem.childLinks.each(link => {
            link.isHover = true;
          });
          if (event.target.dataItem.parentLink) {
            event.target.dataItem.parentLink.isHover = true;
          }
        });
        nodeTemplate.events.on("out", event => {
          event.target.dataItem.childLinks.each(link => {
            link.isHover = false;
          });
          if (event.target.dataItem.parentLink) {
            event.target.dataItem.parentLink.isHover = false;
          }
        });

        interface UserResponse {
          data: Object;
        }

        this.http
          .get<UserResponse>(
            "https://kemlu-v2.onlinemonitoring.id/API/getSNA?tgl_awal=2020-01-01&tgl_akhir=2020-03-10&topic=" +
              this.topicgetdata
          )
          .subscribe(
            (result: any) => {
              result.forEach(element => {
                this.datagroupall.push(element);
              });
              const groupBy = key => array =>
                array.reduce(
                  (objectsByKeyValue, obj) => ({
                    ...objectsByKeyValue,
                    [obj[key]]: (objectsByKeyValue[obj[key]] || []).concat(obj)
                  }),
                  {}
                );
              const groupByAccount = groupBy("account");
              const datajson = {
                groupByAccount: groupByAccount(this.datagroupall)
              };
              const dataFix = datajson["groupByAccount"];
              const datakeyloop = Object.keys(dataFix);
              const datafirst = {
                name: this.topicgetdata,
                value: 10,
                data: this.topicgetdata
              };
              this.pushdataall.push(datafirst);
              for (const key of datakeyloop) {
                const objuse = {
                  name: key,
                  value: 4,
                  linkWith: [this.topicgetdata],
                  data: key,
                  children: dataFix[key].map(el => {
                    let valueuse;
                    if (el.retweet_count < 50) {
                      valueuse = 0;
                    } else if (el.retweet_count > 50) {
                      valueuse = 1;
                    } else {
                      valueuse = 2;
                    }
                    const a = {
                      name: el.retweeter,
                      value: valueuse,
                      linkWith: ["Automotive"],
                      data: el.content
                    };
                    return a;
                  })
                };
                this.pushdataall.push(objuse);
              }
              networkSeries.data = this.pushdataall;
            },
            err => {}
          );
          this.loading = false
        $("select").on("change", () => {
          this.topicgetdata = $("#topicnetwork option:selected").val();
          this.loading = true
          this.datagroupall = [];
          this.pushdataall = [];
          this.http
            .get<UserResponse>(
              "https://kemlu-v2.onlinemonitoring.id/API/getSNA?tgl_awal=2020-01-01&tgl_akhir=2020-03-10&topic=" +
                this.topicgetdata
            )
            .subscribe(
              (result: any) => {
                result.forEach(element => {
                  this.datagroupall.push(element);
                });
                const groupBy = key => array =>
                  array.reduce(
                    (objectsByKeyValue, obj) => ({
                      ...objectsByKeyValue,
                      [obj[key]]: (objectsByKeyValue[obj[key]] || []).concat(
                        obj
                      )
                    }),
                    {}
                  );
                const groupByAccount = groupBy("account");
                const datajson = {
                  groupByAccount: groupByAccount(this.datagroupall)
                };
                const dataFix = datajson["groupByAccount"];
                const datakeyloop = Object.keys(dataFix);
                const datafirst = {
                  name: this.topicgetdata,
                  value: 10,
                  data: this.topicgetdata
                };
                this.pushdataall.push(datafirst);
                for (const key of datakeyloop) {
                  const objuse = {
                    name: key,
                    value: 4,
                    linkWith: [this.topicgetdata],
                    data: key,
                    children: dataFix[key].map(el => {
                      let valueuse;
                      if (el.retweet_count < 50) {
                        valueuse = 0;
                      } else if (el.retweet_count > 50) {
                        valueuse = 1;
                      } else {
                        valueuse = 2;
                      }
                      const a = {
                        name: el.retweeter,
                        value: valueuse,
                        linkWith: ["Automotive"],
                        data: el.content
                      };
                      return a;
                    })
                  };
                  this.pushdataall.push(objuse);
                }
                networkSeries.data = this.pushdataall;
                this.loading = false
              },
              err => {}
            );
        });
      });
  }
}
