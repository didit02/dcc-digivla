import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Panel5Component } from './Panel5.component';



const routes: Routes = [
  { path: '', component: Panel5Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel5RoutingModule { }
