import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Panel5Component } from './Panel5.component';
import { Panel5RoutingModule } from './Panel5-routing.module';


import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

//digivla module
import { CategoryModuleModule } from '../CategoryModule/CategoryModule.module'
import { MediaModuleModule } from '../MediaModule/MediaModule.module'
import { TimePickerModuleModule } from '../TimePickerModule/TimePickerModule.module'
import { ArticlesModuleModule } from '../ArticlesModule/ArticlesModule.module'


@NgModule({
    imports: [
        CommonModule,
        Panel5RoutingModule,
        ChartsModule,
        CategoryModuleModule,
        MediaModuleModule,
        TimePickerModuleModule,
        ArticlesModuleModule,
    ],
    declarations: [Panel5Component],
    providers:[]
})
export class Panel5Module { }
