import { Component, OnInit,ViewChild,ChangeDetectorRef } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { Router } from '@angular/router';
import { ListenerService } from '../Service/Listener.service'
import { Subscription } from 'rxjs';
import { AppComponent } from '../app.component'


@Component({
  selector: 'app-Panel5',
  templateUrl: './Panel5.component.html',
  styleUrls: ['./Panel5.component.scss']
})
export class Panel5Component implements OnInit {
  private subscription: Subscription;

  logo = this.parent.logo;
  url=this.parent.url;
  // token='';
  // headers = new HttpHeaders({
  //   'Content-Type': 'application/json',
  //   'Authorization': this.token});
  // options = { headers: this.headers };
  destroy = true;
  //modal
  @ViewChild('timePickerContainer') public timePickerContainer;
  @ViewChild('articlesContainer') public articlesContainer;
  @ViewChild('mediaContainer') public mediaContainer;

  //filter parameter
  today = new Date();
  periodFromModel :Date = this.addDays(this.today,-7);
  periodEndModel:Date = this.today;
  loaded=false;

  //fitlerparam
  category_set='0';
  user_media_type_id='0';
  timeFrame = 2;  

  // lineChart
  public lineChartData:Array<any> 
  public lineChartLabels:Array<any> 
  public lineChartColors:Array<any>
  public lineChartOptions:any = {
    responsive: true,
    // bezierCurve: false,
    maintainAspectRatio: false,
    legend:{
      display:true,
      position:'bottom',
      labels:{
        fontColor:'whitesmoke'
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: true,          
        },
        ticks: {
          display:false
        }
      }],
      yAxes: [{
        gridLines: {
          drawBorder: false,          

        },
        ticks: {
          display:false
        }
      }]
    }
  };

  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
  
  // events
  public chartClicked(e:any):void {
    if (e.active.length > 0) {
    const chart = e.active[0]._chart;
    const activePoints = chart.getElementAtEvent(e.event);
      if ( activePoints.length > 0) {
        // get the internal index of slice in pie chart
        const clickedElementIndex = activePoints[0]._index;
        const clickDatasetsIndex = activePoints[0]._datasetIndex;
        const label = moment(chart.data.labels[clickedElementIndex]).format('YYYY-MM-DD').toString();
        
        const value = chart.data.datasets[clickDatasetsIndex].data[clickedElementIndex];
        const category = chart.data.datasets[clickDatasetsIndex].label;
        let start = label;
        let end = label;
        if (this.timeFrame ==4){
          var dateConverted = moment(label).toDate();
          dateConverted = this.addDays(dateConverted,30);
          end = (moment(dateConverted).format('YYYY-MM-DD')).toString();
        }
        this.articlesContainer.loadArticles(this.category_set,this.user_media_type_id,'0',category,start,end,1,null);
      }
    }
  }
 
  // public chartHovered(e:any):void {
  //   console.log(e);
  // }
  
  checkKemlu(){
    var domain = window.location.hostname.split('.')[1];
    if (this.parent.subdomain == 'kemlu' && domain == '10.128.10.40'){
      interface UserResponse {
        data: Object;
      }
      this.http.get<UserResponse>('http://10.128.10.40:3001/api/watson/bycategory',this.parent.options)
      .subscribe((result: any) => { 
        let dataGraph=[];
        let xAxisGraph=[];
        let graphColor=[];
        
        for(let i = 0; i < result.data.length;i++){
          let dataSubCategory=[];
          for(let j=0;j<result.data[i].category_id_per_day.buckets.length;j++){          
            dataSubCategory.push(result.data[i].category_id_per_day.buckets[j].doc_count);                  
          }
          dataGraph.push({'data':dataSubCategory,'label':result.data[i].key})
          var color = this.randomNonRedColor();        
          graphColor.push({
            borderColor: color,
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: color
          })
        }
  
        for(let i=0;i < result.data[0].category_id_per_day.buckets.length;i++){
          xAxisGraph.push(result.data[0].category_id_per_day.buckets[i].key_as_string)
        }      
        
        setTimeout(()=>{ this.mouseEnter('time-frame-'+this.timeFrame)},1000);   
        
        this.lineChartData=[]
        this.lineChartData = dataGraph;
        
        this.lineChartLabels=[]
        this.lineChartLabels = xAxisGraph;
        this.lineChartColors=[]
        this.lineChartColors = graphColor;
        this.loaded = true;
      })
    }
  }

  mouseEnter(div : string){
    var className = document.getElementById(div).querySelectorAll("img");
    var re = new RegExp('white', 'gi');     
    className[0].src = className[0].src.replace(re,'primary');
  }

  mouseLeave(div : string){
    // console.log(div.split('-')[2])
    if(div.split('-')[2]!=this.timeFrame.toString()){
      var className = document.getElementById(div).querySelectorAll("img");
      var re = new RegExp('primary', 'gi');     
      className[0].src = className[0].src.replace(re,'white');
    }    
  }

  constructor(private http: HttpClient,private router: Router,private Listener: ListenerService,private parent: AppComponent,private changeDetectorRef: ChangeDetectorRef) { }

  addDays(date, days) {
    var result = new Date(date);
    result.setDate(date.getDate() + days);
    return result;
  }

  loadMediaVisibility(){
    this.loaded =false;
    interface UserResponse {
      data: Object;
    }
    let editingParam={
      "category_set": this.category_set,
      "category_id":'all',
      "user_media_type_id":this.user_media_type_id,
      "media_id": '0',
      "start_date": (moment(this.periodFromModel).format('YYYY-MM-DD')).toString(),
      "end_date": (moment(this.periodEndModel).format('YYYY-MM-DD')).toString()
    };


    this.http.post<UserResponse>(this.url +'dashboard/media-visibility',editingParam,this.parent.options)
    .subscribe((result: any) => {            
      let dataGraph=[];
      let xAxisGraph=[];
      let graphColor=[];
      
      for(let i = 0; i < result.data.length;i++){
        let dataSubCategory=[];
        for(let j=0;j<result.data[i].category_id_per_day.buckets.length;j++){          
          dataSubCategory.push(result.data[i].category_id_per_day.buckets[j].doc_count);                  
        }
        dataGraph.push({'data':dataSubCategory,'label':result.data[i].key})
        var color = this.randomNonRedColor();        
        graphColor.push({
          borderColor: color,
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: color
        })
      }

      for(let i=0;i < result.data[0].category_id_per_day.buckets.length;i++){
        xAxisGraph.push(result.data[0].category_id_per_day.buckets[i].key_as_string)
      }      
      
      setTimeout(()=>{ this.mouseEnter('time-frame-'+this.timeFrame)},1000);   
      
      this.lineChartData=[]
      this.lineChartData = dataGraph;
      
      this.lineChartLabels=[]
      this.lineChartLabels = xAxisGraph;
      this.lineChartColors=[]
      this.lineChartColors = graphColor;
      this.loaded = true;
    });  
  }

  timeClick(time) {
    if(time == 1){      
      // console.log(this.addDays(this.today,-1));
      this.periodFromModel = this.addDays(this.today,-1);
      this.periodEndModel = this.today; 
      
    }else if(time==2){
      this.periodFromModel = this.addDays(this.today,-7);
      this.periodEndModel = this.today; 
      
    }else if(time==3){
      this.periodFromModel = this.addDays(this.today,-30);
      this.periodEndModel = this.today;
      
    }else if(time==4){
      this.periodFromModel = this.addDays(this.today,-360);
      this.periodEndModel = this.today;
      
    }else if(time==5){
      this.timePickerContainer.timePickerModal.show();
    }
    
    this.timeFrame=time;
    this.loadMediaVisibility();
  }

  
  randomNonRedColor = function () {
      //skip green
      var ranges = [[20, 120], [150, 300]];

      //get max random
      var total = 0, i;
      for (i = 0; i < ranges.length; i += 1) {
          total += ranges[i][1]-ranges[i][0] + 1;
      }

      //get random hue index
      var randomHue = Math.floor(Math.random() * total);

      //convert index to actual hue
      var pos = 0;
      for (i = 0; i < ranges.length; i += 1) {
          pos = ranges[i][0];
          if (randomHue + pos <= ranges[i][1]) {
              randomHue += pos;
              break;
          } else {
              randomHue -= (ranges[i][1] - ranges[i][0] + 1);
          }
      }

      return this.hslToHex(randomHue,100,50);
  }

  hslToHex(h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;
    let r, g, b;
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      const hue2rgb = (p, q, t) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }
    const toHex = x => {
      const hex = Math.round(x * 255).toString(16);
      return hex.length === 1 ? '0' + hex : hex;
    };
    return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
  }

  onKey(res:any){
    if (res.code == 'ArrowRight'){      
      this.router.navigateByUrl('/panel6');  
    }else if (res.code == 'ArrowLeft'){
      this.router.navigateByUrl('/panel4');  
    }else if(res.code == 'ArrowDown'){
      this.router.navigateByUrl('/panelDesc');  
    } 
  }

  ngOnInit() {
    sessionStorage.setItem(
      "currentUser",
      JSON.stringify({
        socmed: this.parent.socmed,
      })
    );
      this.loadMediaVisibility();    
      setInterval(()=>{
        this.loadMediaVisibility()
      },60000);
      
    

    this.subscription = this.Listener.notifyObservable$.subscribe((res) => {
      if (res.hasOwnProperty('option') && res.option === 'category') {    
        console    
        this.category_set = res.value;
        this.loadMediaVisibility();
      } else if (res.hasOwnProperty('option') && res.option === 'media') {    
        console    
        this.user_media_type_id = res.value;
        this.loadMediaVisibility();
      } else if (res.hasOwnProperty('option') && res.option === 'time_frame') {    
        console    
        this.periodFromModel = res.value.start;
        this.periodEndModel = res.value.end;
        this.loadMediaVisibility();
      } 
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    // this.destroy=false;
    // this.changeDetectorRef.detectChanges();
  }
}
