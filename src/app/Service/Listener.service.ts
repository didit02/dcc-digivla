import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { map } from "rxjs/operators"; 

@Injectable({
  providedIn: 'root'
})
export class ListenerService {
  private notify = new Subject<any>();
  // private img = new Subject<any>();
  /**
   * Observable string streams
   */
  notifyObservable$ = this.notify.asObservable();
  // passedImg$ = this.img.asObservable();

  constructor(
    private http: HttpClient
  ){}

  public notifyOther(data: any) {
    if (data) {
      this.notify.next(data);
    }
  }

  getlocalgeojson(): Observable<any> {
    return this.http.get('assets/maps/allplacecompress.json').pipe(map(res => res));
  }

}
