import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TweetdeckComponent } from './tweetdeck.component';



const routes: Routes = [
  { 
    path: '', 
    component: TweetdeckComponent 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TweetdeckRoutingModule { }
