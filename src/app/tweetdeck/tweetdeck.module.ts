import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TweetdeckComponent } from './tweetdeck.component';
import { TweetdeckRoutingModule } from './tweetdeckt-routing.module';

@NgModule({
  imports: [
    CommonModule,
    TweetdeckRoutingModule
  ],
  declarations: [TweetdeckComponent]
})
export class TweetdeckModule { }
