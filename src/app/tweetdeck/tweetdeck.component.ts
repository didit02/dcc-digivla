import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tweetdeck',
  templateUrl: './tweetdeck.component.html',
  styleUrls: ['./tweetdeck.component.scss']
})
export class TweetdeckComponent implements OnInit {
  onKey(res:any){
    if (res.code == 'ArrowLeft'){
      this.router.navigateByUrl('/panel3');  
    }else 
    if (res.code == 'ArrowRight'){
      this.router.navigateByUrl('/panel5');  
    }else if(res.code == 'ArrowDown'){
      this.router.navigateByUrl('/panelDesc');  
    } 
  }
  constructor(private router: Router) { }

  ngOnInit() {
  }

}
