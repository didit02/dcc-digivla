import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaModuleComponent } from './MediaModule.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MediaModuleComponent],
  exports:[MediaModuleComponent]
})
export class MediaModuleModule { }
