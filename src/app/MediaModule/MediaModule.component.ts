import { Component, OnInit } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ListenerService } from '../Service/Listener.service'
import { environment } from '../../environments/environment';
// import { Panel5Component } from '../Panel5/Panel5.component'
import { AppComponent } from '../app.component'

@Component({
  selector: 'app-MediaModule',
  templateUrl: './MediaModule.component.html',
  styleUrls: ['./MediaModule.component.scss']
})
export class MediaModuleComponent implements OnInit {
  groupMediaOption;
  mediaSelected;
  elementSelected='media-1';

  constructor(private http: HttpClient,private Listener: ListenerService, private parent: AppComponent) { }
  url=environment.apiUrl;

  mouseEnter(div : string){
    var className = document.getElementById(div);    
    className.style.color = '#52C6D8';
  }

  mouseLeave(div : string){
    if(div != this.elementSelected){
      var className = document.getElementById(div);
      className.style.color = 'whitesmoke';  
    }
  }

  mediaClick(media){
    var className = document.getElementById('media-'+media.iter);    
    className.style.color = '#52C6D8';
    
    if (this.elementSelected != 'media-'+media.iter){
      document.getElementById(this.elementSelected).style.color = 'whitesmoke';
      this.Listener.notifyOther({option: 'media', value: media.user_media_type_id});
    }
    this.elementSelected = 'media-'+media.iter;
    this.mediaSelected = media.user_media_type_id;
    
  }

  getMediaGroup() {
    interface UserResponse {
      data: Object;
    }
    this.http.get<UserResponse>(this.url + 'user/medias/',this.parent.options)
    .subscribe((result: any) => {
      var res = result.results.splice(0,4);
      for (let i =0;i<res.length;i++){
        res[i].iter=i+1;
      }
      this.groupMediaOption = res;  
      // console.log(this.groupMediaOption[0])
      setTimeout(()=>{ this.mediaClick(this.groupMediaOption[0])},1000);   
    });
  }

  ngOnInit() {
    // setTimeout(()=>{ 
      this.getMediaGroup();
    // },1200);  
    
  }

}
