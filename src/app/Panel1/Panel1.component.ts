import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { moment } from 'ngx-bootstrap/chronos/test/chain';
import { options } from '@amcharts/amcharts4/core';

@Component({
  selector: 'app-Panel1',
  templateUrl: './Panel1.component.html',
  styleUrls: ['./Panel1.component.scss']
})
export class Panel1Component implements OnInit {
  totalMention;
  totalReach;
  avCount1;
  avCount2;
  loaded:boolean=false;

  blog;
  facebook;
  forum;
  instagram;
  news;
  twitter;
  video;
  web;

  logo = this.parent.logo;
  // lineChart
  public lineChartData:Array<any> 
  public lineChartLabels:Array<any> 
  public lineChartColors:Array<any>
  public lineChartOptions:any = {
    responsive: true,
    // bezierCurve: false,
    maintainAspectRatio: false,
    legend:{
      display:true,
      position:'bottom',
      labels:{
        fontColor:'whitesmoke'
      }
    },
    scales: {
      xAxes: [{
        gridLines: {
          display: true,          
        },
        ticks: {
          display:false
        }
      }],
      yAxes: [{
        gridLines: {
          drawBorder: false,          

        },
        ticks: {
          display:false
        }
      }]
    }
  };
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  @ViewChild('medsosContainer') public medsosContainer;
  
  constructor(private http: HttpClient,private router: Router,private parent: AppComponent) { }

  public chartClicked(e:any):void {
    if(e.active.length > 0){
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if (activePoints.length > 0){
        const clickedElementIndex = activePoints[0]._index;
        const clickDatasetsIndex = activePoints[0]._datasetIndex;
        const label = moment(chart.data.labels[clickedElementIndex]).format('YYYY-MM-DD').toString();
        const value = chart.data.datasets[clickDatasetsIndex].data[clickedElementIndex];
        const category = chart.data.datasets[clickDatasetsIndex].label;
        const flagPagenation = 'panel2';

        this.medsosContainer.loadMedsos(value,label,label,category,flagPagenation);
      
      }
    }


  }

  kFormatter(num) {
    var suffixes = ["", "K", "M", "B","T"];
    var suffixNum = Math.floor((""+num).length/3);
    suffixNum = suffixNum>1?suffixNum-1:suffixNum
    var shortValue = parseFloat((suffixNum != 0 ? (num / Math.pow(1000,suffixNum)) : num).toPrecision(2));

    if (shortValue % 1 != 0) {
        var shortNum = shortValue.toFixed(1);
    }
    return shortValue+suffixes[suffixNum];
  }

  onKey(res:any){
    if (res.code == 'ArrowRight'){
      this.router.navigateByUrl('/panel2');  
    }else if(res.code == 'ArrowDown'){
      this.router.navigateByUrl('/panelDesc');  
    } 
  }

  loadChartData(){
    this.loaded=false;
    interface UserResponse {
      data: Object;
    }

    // const headers = new HttpHeaders({
    //   'access-token': this.parent.tokenuse,
    //   'project-id': this.parent.projectid
    // });
    // const options = { headers:headers };
    // this.http.get<UserResponse>('https://'+ this.parent.socmed + '.digivla.id/api/onlinemonitoring/' + this.parent.socmed)
    this.http.get<UserResponse>('https://'+ this.parent.socmed + '.onlinemonitoring.id/API/')
    // this.http.get<UserResponse>('https://api.onlinemonitoring.id/digivla/command-center' , options)
    .subscribe((result: any) => {                 
      this.totalMention = this.kFormatter(result.data_page1.total_mentions);
      this.totalReach = this.kFormatter(result.data_page1.reach_day);
      this.avCount1 = this.kFormatter(result.data_page1.mention_day);
      this.avCount2 = this.kFormatter(result.data_page1.mention_hour);

      let dataGraph=[];
      let xAxisGraph=[];
      let graphColor=[];
      
      for(let i = 0; i < result.data_page1.graph.data.length;i++){
        dataGraph.push({'data':result.data_page1.graph.data[i],'label':result.data_page1.graph.series[i]})
        var color = this.randomNonRedColor();        
        graphColor.push({
          borderColor: color,
          pointBorderColor: '#fff',
          pointHoverBackgroundColor: '#fff',
          pointHoverBorderColor: color
        })
      }
      xAxisGraph= result.data_page1.graph.labels;
      
      this.lineChartData=[]
      this.lineChartData = dataGraph;
      this.lineChartLabels=[]
      this.lineChartLabels = xAxisGraph;
      this.lineChartColors=[]
      this.lineChartColors = graphColor;

      const socmed = result.data_page1.breakdown
      const socmed_total = socmed.Blog + socmed.Facebook + socmed.Forum + socmed.Instagram + socmed.News + socmed.Twitter + socmed.Video + socmed.Web;
      this.blog = this.countPercent(socmed.Blog,socmed_total);
      this.facebook = this.countPercent(socmed.Facebook,socmed_total);
      this.forum = this.countPercent(socmed.Forum,socmed_total);
      this.instagram = this.countPercent(socmed.Instagram,socmed_total);
      this.news = this.countPercent(socmed.News,socmed_total);
      this.twitter = this.countPercent(socmed.Twitter,socmed_total);
      this.video = this.countPercent(socmed.Video,socmed_total);
      this.web = this.countPercent(socmed.Web,socmed_total);

      
      this.loaded=true;
    });  
  }

  countPercent(val,total){
    var a = (val/total) *100
      
    return a.toFixed();
  }

  randomNonRedColor = function () {
    //skip green
    var ranges = [[20, 120], [150, 300]];

    //get max random
    var total = 0, i;
    for (i = 0; i < ranges.length; i += 1) {
        total += ranges[i][1]-ranges[i][0] + 1;
    }

    //get random hue index
    var randomHue = Math.floor(Math.random() * total);

    //convert index to actual hue
    var pos = 0;
    for (i = 0; i < ranges.length; i += 1) {
        pos = ranges[i][0];
        if (randomHue + pos <= ranges[i][1]) {
            randomHue += pos;
            break;
        } else {
            randomHue -= (ranges[i][1] - ranges[i][0] + 1);
        }
    }

    return this.hslToHex(randomHue,100,50);
  }

  hslToHex(h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;
    let r, g, b;
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      const hue2rgb = (p, q, t) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }
    const toHex = x => {
      const hex = Math.round(x * 255).toString(16);
      return hex.length === 1 ? '0' + hex : hex;
    };
    return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
  }

  ngOnInit() {
    this.loadChartData();
    setInterval(()=>{
      this.loadChartData();
    },60000);   
  }

}
