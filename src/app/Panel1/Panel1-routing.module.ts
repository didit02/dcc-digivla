import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Panel1Component } from './Panel1.component';



const routes: Routes = [
  { path: '', component: Panel1Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Panel1RoutingModule { }
