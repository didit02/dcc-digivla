import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Panel1Component } from './Panel1.component';
import { Panel1RoutingModule } from './Panel1-routing.module';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import 'chart.piecelabel.js';
import { NgScrollbarModule } from 'ngx-scrollbar';
import {  } from '../ArticlesModule/ArticlesModule.module'
import{ medsosmoduleModule} from '../medsos-module/medsos-module.module'

import { ChartsModule } from 'ng2-charts';
@NgModule({
  imports: [
    CommonModule,
    Panel1RoutingModule,
    ProgressbarModule.forRoot(),
    ChartsModule,
    NgScrollbarModule,
    medsosmoduleModule
  ],
  declarations: [Panel1Component]
})
export class Panel1Module { }
