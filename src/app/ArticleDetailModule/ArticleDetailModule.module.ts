import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleDetailModuleComponent } from './ArticleDetailModule.component';
import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  imports: [
    CommonModule,
    ModalModule
  ],
  declarations: [ArticleDetailModuleComponent],
  exports:[
    ArticleDetailModuleComponent
  ]
})
export class ArticleDetailModuleModule { }
