import { Component, OnInit,ViewChild,ChangeDetectorRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { AppComponent } from '../app.component'

@Component({
  selector: 'app-ArticleDetailModule',
  templateUrl: './ArticleDetailModule.component.html',
  styleUrls: ['./ArticleDetailModule.component.scss']
})
export class ArticleDetailModuleComponent implements OnInit {

  @ViewChild('articleDetailModal') public articleDetailModal: ModalDirective;

  articleArr:any=[];
  titleModel:string;
  contentModel:string;
  dateModel:string;
  mediaModel:string;
  filePdfModel:string;
  mediaExtensionModel:string;
  urlFileModel:string;
  category_id;
  categoriesList:any=[];
  category_idMentioned;

  constructor(private parent: AppComponent,private http: HttpClient,private ref: ChangeDetectorRef) { }

  loadArticle(article,isGeo){
    interface UserResponse {
      data: Object;
    }
     let param = {'article_id': article.article_id}

    this.http.post<UserResponse>(this.parent.url +'user/keywords-by-article-id/',param,this.parent.options)
    .subscribe((result: any) => { 
      var res =[];
      res = result.data;
      var argumenta =[];
      var args = res.join().split(',');   
      for (let i = 0; i< args.length;i++){
        var argsExp = args[i].split('" "')
        if (argsExp.length>1){
          for(let j=0;j<argsExp.length;j++){
            var re = new RegExp('"', 'gi');     
            var rplc = argsExp[j].replace(re, ""); 
            argumenta.push(rplc);
          }
        }else{
          var re = new RegExp('"', 'gi');     
          var rplc = args[i].replace(re, ""); 
          argumenta.push(rplc);
        }        
      }
      highlightedWord = argumenta.join();
      var re = new RegExp(',', 'gi');     
      var highlightedWord = highlightedWord.replace(re, "||");       

      if(isGeo==false){
        this.contentModel = highlightedWord==''? this.highlighted(article.content,article.category_id) : this.highlighted(article.content,highlightedWord);  
      }else{
        this.contentModel = article.content;
      }

      this.filePdfModel = article.file_pdf;
      this.mediaExtensionModel = article.file_pdf.split('.')[1];
      
      this.categoriesList = article.categories.length==0?[article.category_id]:article.categories;
      this.articleArr = article;
      this.titleModel = article.title;
      this.dateModel = article.datee;
      this.mediaModel = article.media_name;

      //url media source
      this.urlFileModel = "http://input.antara-insight.id/media_tv/"+ article.file_pdf.split('-')[0] + "/" + article.file_pdf.split('-')[1] + "/" + article.file_pdf.split('-')[2] + "/" + article.file_pdf;    
      this.articleDetailModal.show();
      this.ref.detectChanges();    
    },(err:any)=>{
    });  
  }
  loadArticlesmap(article){
    interface UserResponse {
      data: Object;
    }
    let params = {
      article_id:article.article_id
    };
    // this.http.post<UserResponse>('./apidigivla/api/v1/article-by-id/',params , this.parent.options)
    this.http.post<UserResponse> ('https://geo.digivla.id/api/v1/article-by-id/', params , this.parent.options )
    .subscribe((result: any) => { 
      var res =[];
      result['data'].forEach(element => {
      //   // let data = `\\"` + element + '"\\';
      if (element.location.includes('Kabupaten')){
        res.push(element.location.split('Kabupaten')[1])
      }else{
        res.push(element.location.split('Kota')[1])
      }
        
      });
      var argumenta =[];
      var args = res.join().split(',');         
      for (let i = 0; i< args.length;i++){
        var argsExp = args[i].split('" "')
        if (argsExp.length>1){
          for(let j=0;j<argsExp.length;j++){
            var re = new RegExp('"', 'gi');     
            var rplc = argsExp[j].replace(re, ""); 
            argumenta.push(rplc);
          }
        }else{
          var re = new RegExp('"', 'gi');     
          var rplc = args[i].replace(re, ""); 
          argumenta.push(rplc);
        }        
      }      
      highlightedWord = argumenta.join();
      var re = new RegExp(',', 'gi');     
      var highlightedWord = highlightedWord.replace(re, "||");       

      this.contentModel = highlightedWord==''? this.highlighted(article.content,article.category_id) : this.highlighted(article.content,highlightedWord);
      
      // this.contentModel = article.content;
      this.filePdfModel = article.file_pdf;
      this.mediaExtensionModel = article.file_pdf.split('.')[1];
      
      // this.categoriesList = article.categories.length==0?[article.category_id]:article.categories;
      this.articleArr = article;
      this.titleModel = article.title;
      this.dateModel = article.datee;
      this.mediaModel = article.media_name;

      //url media source
      this.urlFileModel = "http://input.antara-insight.id/media_tv/"+ article.file_pdf.split('-')[0] + "/" + article.file_pdf.split('-')[1] + "/" + article.file_pdf.split('-')[2] + "/" + article.file_pdf;    
      this.articleDetailModal.show();
      this.ref.detectChanges();    
    },(err:any)=>{
    });  
  }
  highlighted(value: any, args: any): any {
    // let argsArray = args.split('&&')
    let argsArray=[];
    let keyAray=[];

    if (args.includes('&&')) { 
      argsArray = args.split('&&');         
    }else if (args.includes('||')){
      argsArray = args.split('||');
    }else{
      argsArray.push(args);
    }
    for (let i=0; i < argsArray.length;i++){
      var re = new RegExp((argsArray[i].trimEnd()).trimStart(), 'gi');
      value = value.replace(re, "<mark>" + (argsArray[i].trimEnd()).trimStart() + "</mark>");  
    }
    return value;
  }

  ngOnInit() {
  }

}
