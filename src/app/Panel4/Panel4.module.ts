import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Panel4Component } from './Panel4.component';
import { Panel4RoutingModule } from './Panel4-routing.module';

@NgModule({
  imports: [
    CommonModule,
    Panel4RoutingModule
  ],
  declarations: [Panel4Component]
})
export class Panel4Module { }
