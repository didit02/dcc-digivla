import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-Panel4',
  templateUrl: './Panel4.component.html',
  styleUrls: ['./Panel4.component.scss']
})
export class Panel4Component implements OnInit {
  private myTemplate: any = "";
  constructor(private router: Router) { }

  onKey(res:any){
    
    if (res.code == 'ArrowLeft'){
      this.router.navigateByUrl('/panel3');  
    }else 
    if (res.code == 'ArrowRight'){
      this.router.navigateByUrl('/panel5');  
    }else if(res.code == 'ArrowDown'){
      this.router.navigateByUrl('/panelDesc');  
    } 
  }
  ngOnInit() {
    
  }

}
