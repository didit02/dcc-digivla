import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelDescComponent } from './PanelDesc.component';
import { PanelDescRoutingModule } from './PanelDesc-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PanelDescRoutingModule
  ],
  declarations: [PanelDescComponent]
})
export class PanelDescModule { }
