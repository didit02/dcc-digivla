import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PanelDescComponent } from './PanelDesc.component';



const routes: Routes = [
  { 
    path: '', 
    component: PanelDescComponent 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanelDescRoutingModule { }
