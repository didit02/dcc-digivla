import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-PanelDesc',
  templateUrl: './PanelDesc.component.html',
  styleUrls: ['./PanelDesc.component.scss']
})
export class PanelDescComponent implements OnInit {

  handle(route){
    this.router.navigateByUrl(route);  
  }
  constructor(private router: Router) { }

  ngOnInit() {
  }

}
