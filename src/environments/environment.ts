// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //live prod
  apiUrl: 'https://api.digivla.id/api/v1/',
  user: [{
    id: 'kemlu',
    token: 'fb9f251a5a0067467d5211dd3734035972ef192e',
    background: 'assets/img/backgrounds/default@3840x2160.jpg',
    socmed: 'kemlu',
    logo: 'assets/img/kemlu.png',
    tv: '3982',
    cetak: '5110',
    Wordcloud: 'kemlu-v2',
    email: 'rr@antara-insight.id',
    clientid: 'kiuem',
    projectid: 'bWVkbW9uLTcw',
    tokenuse: 'JDJ5JDEwJC4vSnVyeXNXVDZ3U0QwRTZQNmZjL2UxQmhNamR4TnM2Ni9UeEk2OWNra2d4cW5Ib1BjVDJ5'
  },
  {
    id: 'localhost',
    token: '741c8b17eb7beb7a6e67f31adbaf89ce03c611b8',
    background: 'assets/img/backgrounds/default@3840x2160.jpg',
    socmed: 'kemlu',
    logo: 'assets/img/bni.png',
    tv: '5450',
    cetak: '5448',
    Wordcloud: 'kemlu-v2',
    email: 'rr@antara-insight.id',
    clientid: 'gaina',
    projectid: 'bWVkbW9uLTcw',
    tokenuse: 'JDJ5JDEwJC4vSnVyeXNXVDZ3U0QwRTZQNmZjL2UxQmhNamR4TnM2Ni9UeEk2OWNra2d4cW5Ib1BjVDJ5'
  },
  {
    id: 'fujitsu',
    token: '872b4ee8f5f64e559fbe7ffc70223bb362c807cf',
    background: 'assets/img/backgrounds/default@3840x2160.jpg',
    socmed: 'fujitsu',
    logo: 'assets/img/fujitsu.png',
    tv: '5069',
    cetak: '5058',
    Wordcloud: 'fujitsu',
    email: 'rr@antara-insight.id',
    clientid: '2uaof',
    projectid: 'bWVkbW9uLTcw',
    tokenuse: 'JDJ5JDEwJC4vSnVyeXNXVDZ3U0QwRTZQNmZjL2UxQmhNamR4TnM2Ni9UeEk2OWNra2d4cW5Ib1BjVDJ5'
  },
  {
    id: 'bni',
    token: '741c8b17eb7beb7a6e67f31adbaf89ce03c611b8',
    background: 'assets/img/backgrounds/default@3840x2160.jpg',
    socmed: 'bni',
    logo: 'assets/img/bni.png',
    tv: '5450',
    cetak: '5448',
    Wordcloud: 'bni-v2',
    email: 'rr@antara-insight.id',
    clientid: 'gaina',
    projectid: 'bWVkbW9uLTcw',
    tokenuse: 'JDJ5JDEwJC4vSnVyeXNXVDZ3U0QwRTZQNmZjL2UxQmhNamR4TnM2Ni9UeEk2OWNra2d4cW5Ib1BjVDJ5'
  },
  ]
};